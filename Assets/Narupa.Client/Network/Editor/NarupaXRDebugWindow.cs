﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Playback;
using Narupa.VR.Player.Control;
using UnityEditor;
using UnityEngine;

namespace Narupa.Client.Network.Editor
{
    public class NarupaXRDebugWindow : EditorWindow
    {
        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/NarupaXR Debug")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            NarupaXRDebugWindow window = (NarupaXRDebugWindow) EditorWindow.GetWindow(typeof(NarupaXRDebugWindow));
            window.titleContent = new GUIContent("NarupaXR");
            window.Show();
        }

        enum Section
        {
            Network,
            Playback
        }

        private Section tab;

        private void OnGUI()
        {
            tab = (Section) GUILayout.Toolbar((int) tab, Enum.GetNames(typeof(Section)));
            switch (tab)
            {
                case Section.Network:
                    OnNetworkGUI();
                    break;
                case Section.Playback:
                    OnPlaybackGUI();
                    break;
            }
        }

        private void OnNetworkGUI()
        {
            if (Application.isPlaying)
            {
                var networkManager = FindObjectOfType<NetworkManager>();
                var possibleConnections = networkManager.KnownServers;
                EditorGUILayout.LabelField("Available Servers", EditorStyles.boldLabel);
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                foreach (var possibleConnection in possibleConnections)
                    EditorGUILayout.LabelField(possibleConnection.Descriptor,
                        $"{possibleConnection.Address}:{possibleConnection.Port}");
                EditorGUILayout.EndVertical();
                EditorGUILayout.LabelField("Server Information", EditorStyles.boldLabel);
                if (!networkManager.Connected)
                {
                    EditorGUILayout.LabelField("Server Connection", "Not Connected");
                }
                else
                {
                    var server = networkManager.CurrentConnection;
                    if (server != null)
                    {
                        EditorGUILayout.LabelField("Server Connection", server.Connection.ToString());
                        EditorGUILayout.LabelField("Transport Connection", server.TransportConnection.ToString());

                        EditorGUILayout.LabelField("Playback Information", EditorStyles.boldLabel);
                        EditorGUILayout.LabelField("Frames Ready", server.Ready ? "Yes" : "No");
                        EditorGUILayout.LabelField("Frame Count", $"{server.History.Count}");
                    }
                }
            }
        }

        private void OnPlaybackGUI()
        {
            if (Application.isPlaying)
            {
                var playback = FindObjectOfType<VrPlaybackRenderer>();

                if (GUILayout.Button("Load XYZ"))
                {
                    string path = EditorUtility.OpenFilePanel("Load XYZ", "", "xyz");
                    if (!string.IsNullOrEmpty(path))
                    {
                        var trajectory = Narupa.Trajectory.Load.Loader.LoadTrajectory(path, null);
                        if (trajectory != null)
                        {
                            var history = FrameConverter.ConvertTrajectory(trajectory);
                            var provider = new StaticFrameProvider(history);
                            playback.SetFrameProvider(provider);
                            provider.TriggerReady();
                        }
                    }
                }

                var ready = playback.Ready;
                EditorGUILayout.LabelField("Playback Ready", ready ? "Yes" : "No");
                if (ready)
                {
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    EditorGUILayout.LabelField("Frame History", EditorStyles.boldLabel);
                    var history = playback.History;
                    EditorGUILayout.LabelField("First Frame", $"{history.FirstFrame}");
                    EditorGUILayout.LabelField("Final Frame", $"{history.FinalFrame}");
                    EditorGUILayout.LabelField("Frame Count", $"{history.Count}");
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.LabelField("Current Frame", $"{playback.CurrentFrame}");

                    EditorGUILayout.LabelField("Playback Status", playback.PlaybackPlayMode ? "Playing" : "Paused");
                    EditorGUILayout.LabelField("Playback FPS", $"{playback.PlaybackFps}");
                }

                // Server controls
                EditorGUILayout.LabelField("Server Control", EditorStyles.boldLabel);
                EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
                if(GUILayout.Button("Play", EditorStyles.toolbarButton))
                    playback.MediaPlay();
                if(GUILayout.Button("Pause", EditorStyles.toolbarButton))
                    playback.MediaPause();
                if(GUILayout.Button("Reset", EditorStyles.toolbarButton))
                    playback.MediaReset();
                if(GUILayout.Button("Step", EditorStyles.toolbarButton))
                    playback.MediaStep();
                EditorGUILayout.EndHorizontal();
                
                // Playback controls
               

                if (ready)
                {
                    EditorGUILayout.LabelField("Playback Control", EditorStyles.boldLabel);
                    EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
                    if(GUILayout.Button("Play", EditorStyles.toolbarButton))
                        playback.PlaybackPlay();
                    if(GUILayout.Button("Pause", EditorStyles.toolbarButton))
                        playback.PlaybackPause();
                    if(GUILayout.Button("Reset", EditorStyles.toolbarButton))
                        playback.PlaybackReset();
                    if(GUILayout.Button("Step", EditorStyles.toolbarButton))
                        playback.PlaybackNextFrame();
                    EditorGUILayout.EndHorizontal();
                    
                    float newFrame = EditorGUILayout.Slider("Frame", playback.CurrentFrame, playback.History.FirstFrame,
                        playback.History.FinalFrame);
                    if (newFrame != playback.CurrentFrame)
                        playback.PlaybackGotoFrame(newFrame);
                }
            }
        }

        public void Update()
        {
            if (Application.isPlaying)
                Repaint();
        }
    }
}