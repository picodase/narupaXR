﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Playback;
using NSB.Processing;

namespace Narupa.Client.Network
{
    public partial class NarupaServerConnection
    {
        private FrameHistory<NSBFrame> history = new FrameHistory<NSBFrame>();

        public bool IsFrameHistoryReady => Ready && history.Count > 0;

        public event Action FrameHistoryUpdated;

        public event Action FrameHistoryReady;

        public IFrameHistory<NSBFrame> History => history;

        private void AddFrameToHistory(NSBFrame frame)
        {
            lock (history)
            {
                history.AddNewestFrame(frame);
            }

            historyDirty = true;
        }

        private bool historyDirty = false;

        public bool IsFixedHistory => false;
    }
}