﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Nano.Client;
using Narupa.Client.Network.Event;
using UnityEngine;

namespace Narupa.Client.Network
{
    /// <summary>
    ///     Manages enumerating possible servers, and initialising connections once found
    /// </summary>
    public class NetworkManager : MonoBehaviour
    {
        private readonly Queue<SimboxConnectionInfo> discoveredServers = new Queue<SimboxConnectionInfo>();
        private readonly Queue<SimboxConnectionInfo> expiredServers = new Queue<SimboxConnectionInfo>();
        private readonly HashSet<SimboxConnectionInfo> knownServers = new HashSet<SimboxConnectionInfo>();

        private readonly object knownServersLock = new object();

        [SerializeField] private NetworkConfig config;

        [SerializeField] private string configPath;

        private SimboxConnectionInfo currentConnectionInfo;

        private SearchForConnections search;

        private bool searchInProgress;

        /// <summary>
        ///     Event called when a connection to a new server is attempted
        /// </summary>
        public EventHandler<ServerConnectionInitialisedEventArgs> ServerConnectionInitialised;

        private bool shouldReconnect;

        /// <summary>
        ///     Returns a list of known servers
        /// </summary>
        public IEnumerable<SimboxConnectionInfo> KnownServers => knownServers;

        /// <summary>
        ///     The current connection info for the server
        /// </summary>
        public NarupaServerConnection CurrentConnection { get; private set; }

        /// <summary>
        ///     Is the client currently connected to a server
        /// </summary>
        public bool Connected => CurrentConnection != null;

        private bool AutoConnect => config.AutoConnect;

        /// <summary>
        ///     Event called when a new server is discovered
        /// </summary>
        public event EventHandler<ServerDiscoveredEventArgs> ServerDiscovered;

        /// <summary>
        ///     Event called when a server expires
        /// </summary>
        public event EventHandler<ServerExpiredEventArgs> ServerExpired;

        /// <summary>
        ///     Event called when the list of possible servers is changed
        /// </summary>
        public event Action<IEnumerable<SimboxConnectionInfo>> ServerListChanged;

        private void Awake()
        {
            search = new SearchForConnections();
            search.DeviceDiscovered += OnSearchServerDiscovered;
            search.DeviceExpired += OnSearchServerExpired;
        }

        private void Start()
        {
            config = NetworkConfig.Load(configPath);
            if (config.HasCloudConnectionInfo)
                JoinServer(config.CloudConnectionInfo);
            else if (config.IsDirectConnection)
                JoinServer(ParseURI(config.Uri));
            else if (AutoConnect)
                StartCoroutine(AutoconnectToServer());
        }

        private IEnumerator AutoconnectToServer()
        {
            SimboxConnectionInfo info = null;

            var search = new SearchForConnections();
            search.DeviceDiscovered += info_ => info = info_;
            search.BeginSearch();

            while (info == null)
            {
                yield return new WaitForSeconds(0.5f);

                Debug.Log("No servers found.");
            }

            search.EndSearch();

            JoinServer(info);
        }

        private static string GetOpenClientKey()
        {
            byte[] clientKeyBytes =
            {
                227, 189, 198, 86,
                117, 173, 114, 171,
                7, 52, 3, 198,
                37, 220, 61, 79,
                135, 170, 76, 100,
                93, 248, 182, 55,
                196, 170, 86, 165,
                246, 127, 40, 33
            };

            return Convert.ToBase64String(clientKeyBytes);
        }


        private SimboxConnectionInfo ParseURI(string uriStr, string accessToken = null)
        {
            var web = false;
            var uri = new Uri(uriStr);

            if (uri.Scheme == "ws" || uri.Scheme == "http" || uri.Scheme == "https")
                web = true;

            SimboxConnectionInfo info = null;
            if (web)
            {
                if (accessToken == null) accessToken = GetOpenClientKey();
                info = new CloudConnectionInfo("Direct Connection", uri, accessToken);
            }
            else
            {
                info = new NetworkConnectionInfo("Direct TCP Connection", IPAddress.Any, IPAddress.Parse(uri.Host),
                    uri.Port);
            }

            return info;
        }
        
        public event EventHandler<ClientCreatedEventArgs> ClientCreated;  


        public void JoinServer(SimboxConnectionInfo info)
        {
            currentConnectionInfo = info;
            CurrentConnection?.ClearConnection();
            
            CurrentConnection = new NarupaServerConnection(currentConnectionInfo);
            CurrentConnection.ClientCreated += ClientCreated;
            CurrentConnection.ReconnectSignalReceived += OnReconnectSignalReceived;
            
            CurrentConnection.Initialise();
            OnConnectionInitialised(CurrentConnection);
        }

        private void OnConnectionInitialised(IServerConnection connection)
        {
            ServerConnectionInitialised?.Invoke(this, new ServerConnectionInitialisedEventArgs(connection));
        }
        
        private void OnServerDiscovered(SimboxConnectionInfo connection)
        {
            knownServers.Add(connection);
            ServerDiscovered?.Invoke(this, new ServerDiscoveredEventArgs(connection));
        }
        
        private void OnServerExpired(SimboxConnectionInfo connection)
        {
            knownServers.Remove(connection);
            ServerExpired?.Invoke(this, new ServerExpiredEventArgs(connection));
        }

        private void ReconnectToServer()
        {
            if (currentConnectionInfo == null)
                return;
            JoinServer(currentConnectionInfo);
        }

        private void OnReconnectSignalReceived()
        {
            shouldReconnect = true;
        }

        private void Update()
        {
            if (shouldReconnect)
            {
                ReconnectToServer();
                shouldReconnect = false;
            }

            if (discoveredServers.Count > 0 || expiredServers.Count > 0)
            {
                foreach (var discovered in discoveredServers)
                    OnServerDiscovered(discovered);

                foreach (var expired in expiredServers)
                   OnServerExpired(expired);

                ServerListChanged?.Invoke(knownServers);
                discoveredServers.Clear();
                expiredServers.Clear();
            }
        }


        public void StartSearch()
        {
            knownServers.Clear();
            search.BeginSearch();
            searchInProgress = true;
            ServerListChanged?.Invoke(knownServers);
        }

        public void EndSearch()
        {
            knownServers.Clear();
            search.BeginEndSearch();
            searchInProgress = false;
            ServerListChanged?.Invoke(knownServers);
        }

        private void OnSearchServerDiscovered(SimboxConnectionInfo info)
        {
            lock (knownServersLock)
            {
                discoveredServers.Enqueue(info);
            }
        }

        private void OnSearchServerExpired(SimboxConnectionInfo info)
        {
            lock (knownServersLock)
            {
                expiredServers.Enqueue(info);
            }
        }
    }
}