﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using NSB.Simbox;
using Nano.Client;

namespace Narupa.Client.Network
{
    public partial class NarupaServerConnection
    {
        private SimboxSyncBuffer<long> frameStream
            = new SimboxSyncBuffer<long>((int) StreamID.SimulationFrame);

        private SimboxSyncBuffer<BondPair> atomBondPairs
            = new SimboxSyncBuffer<BondPair>((int) StreamID.Bonds);

        private SimboxSyncBuffer_UncompressHalf3ToVector3 atomPositions
            = new SimboxSyncBuffer_UncompressHalf3ToVector3((int) StreamID.AtomPositions);

        private SimboxSyncBuffer_UncompressHalf3ToVector3 atomVelocities
            = new SimboxSyncBuffer_UncompressHalf3ToVector3((int) StreamID.AtomVelocities);

        private SimboxSyncBuffer<float> atomKineticEnergies
            = new SimboxSyncBuffer<float>((int) StreamID.AtomEnergies);
        
        private SimboxSyncBuffer<ushort> atomTypes
            = new SimboxSyncBuffer<ushort>((int) StreamID.AtomTypes);

        private SimboxSyncBuffer<ushort> atomCollisions
            = new SimboxSyncBuffer<ushort>((int) StreamID.AtomCollision);

        private SimboxSyncBuffer<ushort> selectedAtoms
            = new SimboxSyncBuffer<ushort>((int) StreamID.SelectedAtoms);

        private SimboxSyncBuffer<int> visibleAtomMap
            = new SimboxSyncBuffer<int>((int) StreamID.VisibleAtomMap);

        private SimboxSyncBuffer<InteractionForceInfo> interactionForceInfo
            = new SimboxSyncBuffer<InteractionForceInfo>((int) StreamID.InteractionForceInfo);

        private SimboxSyncBuffer<int> topologyUpdateCount
            = new SimboxSyncBuffer<int>((int) StreamID.TopologyUpdate);

        public readonly SimboxSyncBuffer<VRInteraction> VRPositions
            = new SimboxSyncBuffer<VRInteraction>((int) StreamID.VRPositions);

        public List<Nano.Transport.Variables.Interaction.Interaction> Interactions =
            new List<Nano.Transport.Variables.Interaction.Interaction>();
    }
}