// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science;

namespace Narupa.Client.Render.Radius
{
    /// <inheritdoc />
    /// <summary>
    ///     Renderer which uses the VDW radius of an element
    /// </summary>
    public class VdwRadiusRenderer : ElementRadiusRenderer
    {
        /// <inheritdoc />
        protected override float GetElementRadius(ushort element)
        {
            return PeriodicTable.GetElementProperties((Element)element).VDWRadius;
        }
    }
}