// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Client.Render.Context;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     General base class for a renderer which provides a color, with a method for defining a default color
    /// </summary>
    public class ColorRenderer : RendererBehaviour, ITopologyUpdatedHandler
    {
        [RenderProperty("VERTEX.COLOR", WriteOnly = true)]
        private IndexedProperty<Color> atomColor;

        /// <summary>
        ///     The default color to be returned when it cannot be determined for the color-choosing method
        /// </summary>
        /// <returns></returns>
        protected virtual Color GetDefaultColor()
        {
            return Color.grey;
        }

        protected virtual void RefreshColors(IndexedCollection<Color> target)
        {
            
        }

        protected virtual bool Dynamic => false;

        private IndexedCollection<Color> cachedColors = new IndexedCollection<Color>(0);
        
        public override void Refresh() {
            if(Dynamic)
                RefreshColors(cachedColors);
            atomColor.Set(cachedColors);
        }
        

        public void TopologyUpdated()
        {
            RefreshColors(cachedColors);
            atomColor.Set(cachedColors);
        }
    }
}