// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Client.Render.Property;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     Renderer which colors atoms by their topological information (residue id's, atom names etc.)
    /// </summary>
    public abstract class TopologyInfoRenderer : ColorRenderer
    {
        [RenderProperty("VERTEX.INFO", ReadOnly = true, Required = true)]
        private IndexedProperty<TopologyAtom> atomInfo;

        /// <inheritdoc />
        protected override void RefreshColors(IndexedCollection<Color> target)
        {
            target.Resize(atomInfo.Count);
            for (var i = 0; i < atomInfo.Count; i++)
            {
                var info = atomInfo[i];
                target[i] = info != null ? GetAtomColor(info) : GetDefaultColor();
            }
        }

        /// <summary>
        ///     Get the color based on the topology information for the atom
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected abstract Color GetAtomColor(TopologyAtom info);
    }
}