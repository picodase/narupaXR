// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Client;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     Renderer which colors hydrogen bonds by the number of residues spanned by it
    /// </summary>
    public class HydrogenBondColorRenderer : RendererBehaviour
    {
        [RenderProperty("EDGE.COLOR")] private IndexedProperty<Color> bondColor;

        [RenderProperty("EDGES")] private IndexedProperty<BondPair> bonds;

        [RenderProperty("EDGE.HBOND")] private IndexedProperty<int> bondType;

        /// <inheritdoc />
        public override void Refresh()
        {
            base.Refresh();
            bondColor.Resize(bonds.Count);
            for (var i = 0; i < bonds.Count; i++)
            {
                var type = bondType[i];
                bondColor[i] = GetBondColor(type);
            }
        }

        private static Color GetBondColor(int type)
        {
            switch (type)
            {
                case 2:
                    return Color.white;
                case 3:
                    return Color.magenta;
                case 4:
                    return Color.red;
                case 5:
                    return Color.Lerp(Color.red, Color.yellow, 0.5f);
                case -3:
                    return Color.cyan;
                case -4:
                    return Color.green;
                default:
                    return Color.yellow;
            }
        }
    }
}