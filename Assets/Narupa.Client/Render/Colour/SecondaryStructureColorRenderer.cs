using Narupa.Client.Render.Property;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    public class SecondaryStructureColorRenderer : ColorRenderer
    {
        [RenderProperty("VERTEX.SS.TYPE", ReadOnly = true, Required = true)]
        private IndexedProperty<char> atomSsType;

        /// <inheritdoc />
        protected override void RefreshColors(IndexedCollection<Color> target)
        {
            target.Resize(atomSsType.Count);
            for (var i = 0; i < atomSsType.Count; i++)
                target[i] = GetSecondaryStructureColor(atomSsType[i]);
        }

        protected override bool Dynamic => true;

        /// <summary>
        ///     Get the color based on the atom element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected Color GetSecondaryStructureColor(char id)
        {
            switch (id)
            {
                case 'H':
                    return Color.green;
                case 'G':
                    return Color.yellow;
                case 'I':
                    return Color.red;
                default:
                    return Color.gray;
            }
        }
    }
}