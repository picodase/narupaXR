﻿using Nano.Science;
using Narupa.Client.Render.Definition;
using UnityEditor;
using UnityEngine;

namespace Narupa.Client.Render.Editor
{
	[CustomPropertyDrawer(typeof(AtomColorDictionary.AtomColorKeyValuePair))]
	public class AtomColorKeyValuePairPropertyDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var w = 96;
			var w2 = 64;
			var colorRect = new Rect(position.xMax-w, position.y, w, position.height);
			var numberRect = new Rect(position.xMax-w-w2-8, position.y, w2, position.height);
			var labelRect = new Rect(position.x, position.y, position.width-w-w2-16, position.height);
			var elementId = property.FindPropertyRelative("AtomicNumber").intValue;
			Element? element = null;
			if (elementId > 0 && elementId < (int)Element.MAX)
				element = (Element) elementId;
			if(element.HasValue)
				EditorGUI.LabelField(labelRect, element.ToString());
			EditorGUI.PropertyField(numberRect, property.FindPropertyRelative("AtomicNumber"), GUIContent.none);
			EditorGUI.PropertyField(colorRect, property.FindPropertyRelative("Color"), GUIContent.none);
		}
	}
}
