// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Reflection;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Narupa.Client.Render.Context
{
    /// <inheritdoc />
    /// <summary>
    ///     Renders some kind of data by calling a set of child RendererBehaviour's, which share data using fields
    ///     marked with the RenderProperty attribute. When initialised, a RenderContext sets up these to be the same
    ///     variable
    /// </summary>
    public abstract class RenderContext : MonoBehaviour
    {
        private readonly Dictionary<string, object> properties = new Dictionary<string, object>();

        private readonly List<RendererBehaviour> renderers = new List<RendererBehaviour>();

        /// <summary>
        ///     List of all RendererBehaviour's which are used by this context. The order is important as they are
        ///     refreshed sequentially
        /// </summary>
        protected IEnumerable<RendererBehaviour> Renderers => renderers;

        /// <summary>
        ///     Clears all properties and repopulate the list by searching this class and any child RendererBehaviours
        ///     for fields marked using [RenderProperty]
        /// </summary>
        public void SetupContext()
        {
            // Find all renderers which are children of this game object
            renderers.Clear();
            renderers.AddRange(GetComponentsInChildren<RendererBehaviour>());

            properties.Clear();

            // Find properties in this class
            FindProperties(this);

            // Find properties in each of the child renderers
            foreach (var childRenderer in Renderers)
                FindProperties(childRenderer);
        }

        private void OnEnable()
        {
            SetupContext();
        }

        private void Update()
        {
            Refresh();
        }

        /// <summary>
        ///     Called each frame to update the renderer
        /// </summary>
        public virtual void Refresh()
        {
            foreach (var childRenderer in GetComponentsInChildren<RendererBehaviour>()) childRenderer.Refresh();
        }

        /// <summary>
        ///     Find all [RenderProperty] marked fields on the object 'obj', considering the type 'type'
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        private void FindProperties(object obj, Type type = null)
        {
            // Recurse through the class tree until object
            while (type != typeof(object))
            {
                // If not initialised with a type, set the type to the actual type of the object
                if (type == null) type = obj.GetType();

                // Iterate through all instance fields, both public and private
                foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic |
                                                     BindingFlags.Public))
                {
                    // Ignore the field if it does not have a RenderProperty attribute
                    var attribute = field.GetCustomAttribute(typeof(RenderProperty)) as RenderProperty;
                    if (attribute == null) continue;


                    object existing;
                    if (properties.TryGetValue(attribute.Id, out existing))
                    {
                        // Property already created, set member to reference existing implementation
                        field.SetValue(obj, existing);
                    }
                    else
                    {
                        // Property not defined, create an instance using the type's default constructor
                        if (attribute.Required)
                            Debug.LogError($"Renderer of type {type} requires property {attribute.Id}", obj as Object);
                        var val = Activator.CreateInstance(field.FieldType);
                        properties.Add(attribute.Id, val);
                        field.SetValue(obj, val);
                    }
                }

                // Look for properties that are in the base class
                type = type.BaseType;
            }
        }
    }
}