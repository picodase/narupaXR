// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using Nano.Client;
using Narupa.Client.Config;
using Narupa.Client.Render.Context;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;

namespace Narupa.Client.Render.Calculator
{
    /// <inheritdoc />
    /// <summary>
    ///     Calculates all small cycles in a graph. Used for finding aromatic rings
    /// </summary>
    public class CycleCalculator : RendererBehaviour, ITopologyUpdatedHandler
    {
        [RenderProperty("CYCLES")] private IndexedProperty<Cycle> cycles;

        [RenderProperty("EDGES", ReadOnly = true)] private IndexedProperty<BondPair> edges;

        public override void Refresh()
        {
            cycles.Set(cachedCycles);
        }

        private List<Cycle> cachedCycles = new List<Cycle>();
        
        private int[,] graph;

        public void TopologyUpdated()
        {
            FindRings();
            Refresh();
        }

        /// <summary>
        /// Finds all rings in the current frame
        /// </summary>
        private void FindRings()
        {
            graph = new int[edges.Count, 2];
            for (var k = 0; k < edges.Count; k++)
            {
                graph[k, 0] = edges[k].A;
                graph[k, 1] = edges[k].B;
            }

            FindCycles();
        }

        /// <summary>
        ///     Finds a set of cycles in a graph. Copied from https://stackoverflow.com/a/14115627
        /// </summary>
        private void FindCycles()
        {
            cachedCycles.Clear();
            for (var i = 0; i < graph.GetLength(0); i++)
            for (var j = 0; j < graph.GetLength(1); j++)
                FindNewCycles(cachedCycles, new[] {graph[i, j]});
        }

        private void FindNewCycles(ICollection<Cycle> cyclesList, int[] path)
        {
            if (path.Length > 6)
                return;
            var n = path[0];
            var sub = new int[path.Length + 1];

            for (var i = 0; i < graph.GetLength(0); i++)
            for (var y = 0; y <= 1; y++)
                if (graph[i, y] == n)
                    //  edge refers to our current node
                {
                    var x = graph[i, (y + 1) % 2];
                    if (!Visited(x, path))
                        //  neighbor node not on path yet
                    {
                        sub[0] = x;
                        Array.Copy(path, 0, sub, 1, path.Length);
                        //  explore extended path
                        FindNewCycles(cyclesList, sub);
                    }
                    else if (path.Length > 2 && x == path[path.Length - 1])
                        //  cycle found
                    {
                        var p = Normalize(path);
                        var inv = Invert(p);
                        if (IsNew(cyclesList, p) && IsNew(cyclesList, inv))
                            cyclesList.Add(new Cycle(p));
                    }
                }
        }

        private static bool ListEquals(IReadOnlyList<int> a, IReadOnlyList<int> b)
        {
            if (a.Count != b.Count)
                return false;
            for(var i = 0; i < a.Count; i++)
                if (a[i] != b[i])
                    return false;
            return true;
        }

        private static int[] Invert(IReadOnlyList<int> path)
        {
            var p = new int[path.Count];

            for (var i = 0; i < path.Count; i++)
                p[i] = path[path.Count - 1 - i];

            return Normalize(p);
        }

        //  rotate cycle path such that it begins with the smallest node
        private static int[] Normalize(int[] path)
        {
            var p = new int[path.Length];
            var x = Smallest(path);

            Array.Copy(path, 0, p, 0, path.Length);

            while (p[0] != x)
            {
                var n = p[0];
                Array.Copy(p, 1, p, 0, p.Length - 1);
                p[p.Length - 1] = n;
            }

            return p;
        }

        private static bool IsNew(IEnumerable<Cycle> cyclesList, IReadOnlyList<int> path)
        {
            return !cyclesList.Any(p => ListEquals(p, path));
        }

        private static int Smallest(IEnumerable<int> path)
        {
            return path.Min();
        }

        private static bool Visited(int n, IEnumerable<int> path)
        {
            return path.Any(p => p == n);
        }

        [Serializable]
        public class Config : BaseConfig
        {
            public IntParameter MaxCycleSize;

            protected override void SetupParameters()
            {
                MaxCycleSize.SetRange(3, 32);
            }
        }
    }
}