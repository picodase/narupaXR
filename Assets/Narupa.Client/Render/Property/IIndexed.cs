// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.Client.Render.Property
{
    /// <inheritdoc />
    /// <summary>
    ///     Indexed object of type T, where each element can be both read and written
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIndexed<T> : IReadOnlyList<T>
    {
        new T this[int i] { get; set; }
    }
}