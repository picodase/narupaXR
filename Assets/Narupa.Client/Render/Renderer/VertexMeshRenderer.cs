﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Narupa.Client.Config;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using NSB.Utility;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Narupa.Client.Render.Renderer
{
    public class VertexMeshRenderer : VertexRenderer
    {
        private readonly ParticleBatchSet particleBatches = new ParticleBatchSet();
        private readonly ParticleBatchRenderer particleRenderer = new ParticleBatchRenderer();
        private readonly List<Vector3> rotations = new List<Vector3>();

        private Mesh mesh;

        private bool meshDirty;

        [Header("Setup")] public Config Settings = new Config();

        [HideInInspector] [SerializeField] private Material vdwMaterial;

        [Header("Shaders")] [SerializeField] private Shader vdwShader;

        private void Awake()
        {
            mesh = new Mesh();

            if (vdwMaterial == null)
                vdwMaterial = new Material(vdwShader);

            Settings.MeshComplexity.ValueChanged += (sender, args) => meshDirty = true;
            Settings.HemisphereCull.ValueChanged += (sender, args) => meshDirty = true;
            Settings.Scaling.ValueChanged += (sender, args) => meshDirty = true;

            RegenerateMesh();
        }

        private void RandomiseRotations()
        {
            rotations.Resize(64);
            for (var i = 0; i < rotations.Count; i++)
                if (!Settings.Hemispheres)
                    rotations[i] = Random.rotationUniform.eulerAngles;
                else
                    rotations[i] = (Quaternion.Euler(-90, 0, 0) * Quaternion.Euler(0, Random.value * 360, 0))
                        .eulerAngles;
        }

        public override void Refresh()
        {
            if (meshDirty) RegenerateMesh();

            if (Settings.Spin)
                RandomiseRotations();

            particleBatches.GenerateParticles(this, atomPosition.Count, UpdateParticle);
            particleRenderer.RenderBatchSet(particleBatches);
        }

        private static void UpdateParticle(VertexMeshRenderer data,
            int id,
            ref ParticleSystem.Particle particle)
        {
            particle.position = data.atomPosition[id];
            particle.startColor = data.atomColor[id];
            particle.startSize3D = Vector3.one * data.atomRadius[id] * data.Settings.Scaling.Value;
            particle.rotation3D = data.rotations[id % data.rotations.Count];
        }

        private void RegenerateMesh()
        {
            meshDirty = false;
            MeshGeneration.GenerateAtomSphere(mesh,
                Settings.MeshComplexity,
                hemisphere: Settings.Hemispheres,
                hemisphereCull: Settings.HemisphereCull);

            particleBatches.SetBatchSize(ParticleBatchRenderer.VertexLimit / mesh.vertexCount - 1);
            particleRenderer.Initialise(transform);
            particleRenderer.SetMesh(mesh, Settings.Hemispheres);
            particleRenderer.SetMaterial(vdwMaterial);

            RandomiseRotations();
        }

        [Serializable]
        public class Config : BaseConfig
        {
            public IntParameter Darken = 64;
            public FloatParameter HemisphereCull = 0;
            public BoolParameter Hemispheres = false;
            public IntParameter MeshComplexity = 6;
            public FloatParameter Scaling = 1f;
            public BoolParameter Spin = false;

            protected override void SetupParameters()
            {
                Darken.SetRange(0, 255);
                MeshComplexity.SetRange(0, 12);
                HemisphereCull.SetRange(0, 1);
                Scaling.SetRange(0f, 1f);
            }
        }
    }
}