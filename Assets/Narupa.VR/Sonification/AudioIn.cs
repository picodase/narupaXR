﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Sonification
{
    public class AudioIn : MonoBehaviour
    {
        //public Microphone mic;
        public AudioSource audioSource;
    

        // Use this for initialization
        void Start ()
        {
            audioSource.clip = Microphone.Start("Built In Microphone", true, 10, 44100);
            audioSource.loop = true;

            while (!(Microphone.GetPosition(null) > 0))
            {

            }

            audioSource.Play();
        }

        // Update is called once per frame
        void Update()
        {
            float[] spectrum = new float[256];

            audioSource.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);

            for (int i = 1; i < spectrum.Length - 1; i++)
            {
                //Debug.Log(spectrum[i]);
            }
        }
    }
}
