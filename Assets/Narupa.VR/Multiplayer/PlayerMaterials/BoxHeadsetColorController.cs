// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Multiplayer.PlayerMaterials
{
    
    /// <summary>
    ///     Controls the color of the standard box headset
    /// </summary>
    public class BoxHeadsetColorController : MonoBehaviour
    {
        private readonly string colorShaderString = "_OutlineColor";

        private MultiplayerObjectMaterialController objectMaterialController;

        
        private void Awake()
        {
            objectMaterialController = GetComponent<MultiplayerObjectMaterialController>();
            objectMaterialController.PaletteUpdated += ObjectMaterialControllerOnPaletteUpdated;
        }

        private void ObjectMaterialControllerOnPaletteUpdated(object sender, MultiplayerObjectMaterialController.ColorPaletteUpdatedEventArgs e)
        {
            foreach (var renderer in this.GetComponentsInChildren<Renderer>())
                renderer.material.SetColor(colorShaderString, e.Primary);
        }
      
    }
}