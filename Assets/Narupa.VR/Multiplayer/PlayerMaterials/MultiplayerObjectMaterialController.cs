﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.MaterialPalette;
using UnityEngine;

namespace Narupa.VR.Multiplayer.PlayerMaterials
{
    /// <summary>
    ///     Class for controlling the material of a multiplayer object, using the colors provided by
    ///     a global MultiplayerMaterialController
    /// </summary>
    [RequireComponent(typeof(MultiplayerObjectController))]
    public class MultiplayerObjectMaterialController : MonoBehaviour
    {
  
        public class ColorPaletteUpdatedEventArgs : EventArgs
        {
            public Color Primary;
            public Color Secondary;

            public ColorPaletteUpdatedEventArgs(Color primary, Color secondary)
            {
                this.Primary = primary;
                this.Secondary = secondary;
            }
        }
        
        [SerializeField] private MultiplayerObjectController objectController;
        
        [SerializeField] private GlobalMaterialPalette materialPalette;


        private MultiplayerMaterialController materialController;

        public event EventHandler<ColorPaletteUpdatedEventArgs> PaletteUpdated;

        private void Awake()
        {
            objectController = GetComponent<MultiplayerObjectController>();
            materialController = FindObjectOfType<MultiplayerMaterialController>();
            objectController.PlayerIdUpdated += OnPlayerIdUpdated;
        }

        private void OnEnable()
        {
            UpdatePalette(-1);
        }

        private void OnPlayerIdUpdated(int id)
        {
            UpdatePalette(id);
        }

        private void UpdatePalette(int playerId)
        {
            var palette = FindObjectOfType<GlobalMaterialPalette>().Palette;
            var color = materialController.GetPlayerColor(playerId);
            var evnt = new ColorPaletteUpdatedEventArgs(playerId != -1 ? color : palette.PrimaryColor, palette.SecondaryColor);
            PaletteUpdated?.Invoke(this, evnt);
        }

    }
}