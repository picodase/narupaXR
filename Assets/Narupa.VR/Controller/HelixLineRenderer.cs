﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;

namespace Narupa.VR.Controller
{
	[RequireComponent(typeof(LineRenderer))]
	public class HelixLineRenderer : MonoBehaviour
	{

		public float Height = 1f;

		public float Radius = 0.2f;

		public float Coils = 1f;

		public float SegmentLength = 0.1f;
		
		// Use this for initialization
		void UpdateLineRenderer ()
		{
			SegmentLength = Mathf.Max(SegmentLength, 0.001f);
			var width = Radius * 2 * Mathf.PI * Coils;
			var length = Math.Sqrt(Height * Height + width * width);

			var segments = 2 + (int) (length / SegmentLength);
			segments = Mathf.Min(segments, 1000);

			var d = 1f / segments;
			var da = Coils * Mathf.PI * 2 * d;
			var dh = d * Height;
			Vector3[] pts = new Vector3[segments+1];
			for (var i = 0; i <= segments; i++)
			{
				var A = da * i;
				var H = dh * i;
				pts[i] = new Vector3(Mathf.Cos(A) * Radius, Mathf.Sin(A) * Radius, H);
			}

			GetComponent<LineRenderer>().positionCount = segments+1;
			GetComponent<LineRenderer>().SetPositions(pts);
		}
	
		// Update is called once per frame
		void OnValidate () {
			UpdateLineRenderer();
		}
	}
}
