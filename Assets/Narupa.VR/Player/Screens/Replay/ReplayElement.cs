﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;
using Text = TMPro.TextMeshProUGUI;

namespace Narupa.VR.Player.Screens.Replay
{
    /// <summary>
    /// Class for selecting replays.
    /// </summary>
    public class ReplayElement : InstanceView<ReplayInfo>
    {
        [SerializeField] private Text descriptionText;

        [SerializeField] private VrReplaySelectionPanel panel;

        [SerializeField] private Button selectButton;

        private void Awake()
        {
            selectButton.onClick.AddListener(() => panel.SelectReplay(config));
        }

        /// <inheritdoc />
        protected override void Configure()
        {
            descriptionText.text = config.Asset.name;
        }
    }
}