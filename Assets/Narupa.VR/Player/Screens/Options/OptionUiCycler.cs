﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.Player.Screens.Options
{
    /// <summary>
    /// Class for cycling through options in the UI.
    /// </summary>
    public class OptionUiCycler : MonoBehaviour
    {
        private enum BoolOption
        {
            Off,
            On
        }

        //TODO generalise this to objects or types?
        private readonly List<string> optionValues = new List<string>();

        [SerializeField] private Button leftButton;

        [SerializeField] private TextMeshProUGUI optionLabel;

        /// <summary>
        ///     Which option we're currently pointing at.
        /// </summary>
        private int optionPointer;

        [SerializeField] private TextMeshProUGUI optionValue;

        [SerializeField] private Button rightButton;

        private int OptionCount => optionValues.Count;

        /// <summary>
        /// Event triggered when an option changes.
        /// </summary>
        public event EventHandler OptionChanged;

        // Use this for initialization
        private void Start()
        {
            leftButton.onClick.AddListener(OnLeftClick);
            rightButton.onClick.AddListener(OnRightClick);
        }

        private void OnLeftClick()
        {
            DecrementOptionPointer();
            UpdateSelectedOption();
        }

        private void OnRightClick()
        {
            IncrementOptionPointer();
            UpdateSelectedOption();
        }

        /// <summary>
        /// Set an option to a given option index.
        /// </summary>
        /// <param name="optionIndex">Index of the option.</param>
        public void SetOption(int optionIndex)
        {
            optionPointer = optionIndex;
            UpdateSelectedOption();
        }

        /// <summary>
        /// Set an option to a given option value, if valid.
        /// </summary>
        /// <param name="value">Value of the option.</param>
        public void SetOption(string value)
        {
            if (optionValues.Contains(value))
            {
                optionPointer = optionValues.IndexOf(value);
                UpdateSelectedOption();
            }
        }

        private void UpdateSelectedOption()
        {
            var value = optionValues[optionPointer];
            optionValue.text = value;
            if (OptionChanged != null) OptionChanged(value, null);
        }

        private void DecrementOptionPointer()
        {
            if (optionPointer == 0)
                optionPointer = OptionCount - 1;
            else
                optionPointer--;
        }

        private void IncrementOptionPointer()
        {
            if (optionPointer == OptionCount - 1)
                optionPointer = 0;
            else
                optionPointer++;
        }

        /// <summary>
        /// Registers an option with a given label and list of valid values.
        /// </summary>
        /// <param name="label">The option label.</param>
        /// <param name="values">The valid option values.</param>
        public void RegisterOption(string label, List<string> values)
        {
            RegisterOptionLabel(label);
        }

        /// <summary>
        /// Register an option of the given type.
        /// </summary>
        /// <param name="label">Label of the option.</param>
        /// <typeparam name="T">Type of values this option accepts.</typeparam>
        public void RegisterOption<T>(string label)
        {
            RegisterOptionLabel(label);
            RegisterOptionValues<T>();
        }

        /// <summary>
        /// Registers a boolean valued option.
        /// </summary>
        /// <param name="label">Label of the option.</param>
        public void RegisterBooleanOption(string label)
        {
            RegisterOptionLabel(label);
            RegisterBooleanValues();
        }

        private void RegisterOptionLabel(string label)
        {
            optionLabel.text = label;
        }

        private void RegisterOptionValues(IEnumerable<string> values)
        {
            optionValues.Clear();
            optionValues.AddRange(values);
            UpdateSelectedOption();
        }

        private void RegisterOptionValues<T>()
        {
            if (typeof(T).BaseType == typeof(Enum))
            {
                optionValues.Clear();
                var types = Enum.GetValues(typeof(T));
                foreach (var type in types) optionValues.Add(type.ToString());
            }

            UpdateSelectedOption();
        }

        /// <summary>
        /// Get the string associated with a given boolean value.
        /// </summary>
        /// <param name="value">The boolean value</param>
        /// <returns>The string associated with boolean values. </returns>
        public static string GetBoolOptionString(bool value)
        {
            return value ? BoolOption.On.ToString() : BoolOption.Off.ToString();
        }

        private void RegisterBooleanValues()
        {
            RegisterOptionValues<BoolOption>();
        }

        /// <summary>
        /// Gets the boolean value of a string associated with a boolean option.
        /// </summary>
        /// <param name="optionVal">The string value (On/Off).</param>
        /// <returns>Bool associated with the value.</returns>
        public static bool GetBoolFromOptionString(string optionVal)
        {
            var option = (BoolOption) Enum.Parse(typeof(BoolOption), optionVal);
            return option == BoolOption.On;
        }
    }
}