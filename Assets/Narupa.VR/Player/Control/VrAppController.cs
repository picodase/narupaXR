﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Nano.Client;
using Narupa.Client.Network;
using Narupa.Client.Network.Event;
using Narupa.VR.Selection;
using Narupa.VR.UI;
using NSB.Examples.Player;
using NSB.Examples.Player.Screens.Replay_Select;
using NSB.Examples.Player.Screens.Server_Select;
using NSB.Examples.Utility;
using NSB.MMD.Base;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Narupa.VR.Player.Control
{
    /// <summary>
    ///     Big blob of code that can be used to control transitions between UI.
    /// </summary>
    public class VrAppController : MonoBehaviour
    {
        private NetworkManager networkManager;
        
        /// <summary>
        ///     Whether clicking on an atom will result in selection or interaction.
        /// </summary>
        public static bool SelectionMode;

        private static PointerEventData _pointer;
        private static readonly List<RaycastResult> Hits = new List<RaycastResult>();

        private AtomColliderEventInfo atomSelectedInfo;

        private RendererManager baseRenderering;

        /// <summary>
        /// Indicates whether to output to the console reporter.
        /// </summary>
        public bool ConsoleReporterOutput;
        private VrMenuButtonController controllerUi;

        [SerializeField] private GameObject debugLog;

        [SerializeField] private ErrorPopup errorPopup;

        [Header("VR Controllers")] [SerializeField]
        private GameObject leftVrController;

        [Header("Screens")] [SerializeField] private MainMenuCanvas mainMenuCanvas;

        [SerializeField] private GameObject mainMenuObject;
        public VrPlaybackRenderer Playback;


        private VrRadialMenuRoot radialMenuRoot;
        private bool reconnect;

        [SerializeField] private ReplaySelectionPanel replaySelect;

        [SerializeField] private GameObject rightVrController;
        private SelectionManager selectionManager;

        [SerializeField] private ServerSelectionPanel serverSelect;

        [SerializeField] private LocalSimulationSelectionPanel simulationSelect;


        //Trajectory file to load into Nano Simbox
        [SerializeField]
        private TextAsset trajectoryFile;
        [SerializeField]
        private bool runTrajectory;

        private void Awake()
        {
            networkManager = FindObjectOfType<NetworkManager>();
            networkManager.ServerConnectionInitialised += EnterServer;
        }

        private void Start()
        {
            selectionManager = FindObjectOfType<SelectionManager>();

            baseRenderering = Playback.GetComponentInChildren<RendererManager>();
            Playback.OnReady += OnPlaybackReady;
            
            if (rightVrController != null)
            {
                radialMenuRoot = rightVrController.GetComponentInChildren<VrRadialMenuRoot>();
            }

            if (leftVrController != null)
                controllerUi = leftVrController.GetComponentInChildren<VrMenuButtonController>();
            if(controllerUi != null) controllerUi.EnableMenuButton(false);
            if(radialMenuRoot != null) radialMenuRoot.gameObject.SetActive(false);
            

            TryLoadTrajectory();
        }
        

        private void OnPlaybackReady()
        {
            Playback.VisibleAtoms.Selection.SetAll(true);
            //Ensure base layer renderering is enabled.
            baseRenderering.gameObject.SetActive(true);
        }

        /// <summary>
        /// Configures the scene to have no playback.
        /// </summary>
        public void SetNoPlayback()
        {
            Playback.Clear();

            //Playback.gameObject.SetActive(false);
        }

        /// <summary>
        /// Sets up a single instance of the molecular playback.
        /// </summary>
        public void SetSinglePlayback()
        {
            Playback.gameObject.SetActive(true);
        }

        /// <summary>
        /// Shows an error message.
        /// </summary>
        /// <param name="message">Message to show.</param>
        /// <param name="cancel">Cancel message.</param>
        public void ErrorMessage(string message, string cancel = "Ignore")
        {
            errorPopup.Show($"Something went wrong.\n\n{message}",
                new ButtonEntry(cancel, errorPopup.Hide),
                new ButtonEntry("See Log", OpenDebugLog));
        }

        /// <summary>
        /// Shows a network error.
        /// </summary>
        /// <param name="message">Message to show.</param>
        /// <param name="retry">Message to show for retrying.</param>
        /// <param name="ignore">Message to show for ignoring.</param>
        public void NetworkError(string message,
            Action retry,
            Action ignore = null)
        {
            errorPopup.Show($"Couldn't connect to server.\n\n{message}",
                new ButtonEntry("Ignore", () =>
                {
                    ignore?.Invoke();
                    errorPopup.Hide();
                }),
                new ButtonEntry("Retry", () =>
                {
                    retry();
                    errorPopup.Hide();
                }));
        }

        /// <summary>
        /// Opens the debug log.
        /// </summary>
        public void OpenDebugLog()
        {
            debugLog.SetActive(true);
        }

        private void HideAll()
        {
            if (mainMenuObject != null) mainMenuObject?.gameObject.SetActive(false);
            serverSelect?.gameObject.SetActive(false);
            replaySelect?.gameObject.SetActive(false);
            simulationSelect?.gameObject.SetActive(false);
        }

        private void HideCanvas()
        {
            if (mainMenuCanvas != null) mainMenuCanvas?.gameObject.SetActive(false);
        }

        private void ShowCanvas()
        {
            if (mainMenuCanvas != null) mainMenuCanvas?.gameObject.SetActive(true);
        }

        private void ShowVrUi(bool enable)
        {
            if (controllerUi != null)
            {
                controllerUi.EnableMenuButton(enable);
                controllerUi.ShowVrMenu(enable);
            }
            //Enable gradient scale factor slider controls.
            if (radialMenuRoot != null) radialMenuRoot.gameObject.SetActive(enable);
        }

        /// <summary>
        /// Enters the server selection screen.
        /// </summary>
        public void EnterServerSelection()
        {
            SetNoPlayback();
            HideAll();
            serverSelect.gameObject.SetActive(true);
            ShowVrUi(false);
        }

        /// <summary>
        /// Enters the replay selection screen.
        /// </summary>
        public void EnterReplaySelection()
        {
            SetNoPlayback();
            HideAll();
            replaySelect.gameObject.SetActive(true);
            ShowVrUi(false);
        }

        /// <summary>
        /// Enters the simulation selection screen.
        /// </summary>
        public void EnterSimulationPathSelection()
        {
            SetNoPlayback();
            HideAll();
            simulationSelect.gameObject.SetActive(true);
        }

        /// <summary>
        /// Enters the given Narupa Server.
        /// </summary>
        /// <param name="inf">The Simbox Server to connect to.</param>
        private void EnterServer(object sender, ServerConnectionInitialisedEventArgs args)
        {
            HideAll();
            HideCanvas();
            SetSinglePlayback();
            ShowVrUi(true);
        }

        /// <summary>
        /// Enters main menu.
        /// </summary>
        public void EnterMainMenu()
        {
            SetNoPlayback();
            HideAll();
            ShowCanvas();
            mainMenuObject?.gameObject.SetActive(true);
        }

        /// <summary>
        /// Enters the given recording.
        /// </summary>
        /// <param name="asset">Text asset containing simulation trajectory.</param>
        public void EnterRecording(TextAsset asset)
        {
            HideAll();
            HideCanvas();
            SetSinglePlayback();
            Playback.EnterRecording(asset.bytes);
            ShowVrUi(true);
        }

        /// <summary>
        /// Enters the given recording.
        /// </summary>
        /// <param name="asset">Raw byte data containing simulation trajectory.</param>
        public void EnterRecording(byte[] bytes)
        {
            HideAll();
            HideCanvas();
            SetSinglePlayback();
            Playback.EnterRecording(bytes);
            ShowVrUi(true);
        }

        /// <summary>
        /// Checks if the user has passed an external trajectory and, if so, loads it into the virtual space 
        /// </summary>
        public void TryLoadTrajectory()
        {
            var index = Array.IndexOf(System.Environment.GetCommandLineArgs(), "-trajectory");

            if (runTrajectory && trajectoryFile != null)
            {
                EnterRecording(trajectoryFile);
            }
            else if (index > -1)
            {
                //try to load in trajectory from file 
                string[] args = System.Environment.GetCommandLineArgs();
                string trajPath = args[index + 1];
                byte[] trajData = File.ReadAllBytes(trajPath);
                EnterRecording(trajData);
            }
        }

        /// <summary>
        /// Determines whether the pointer is blocked.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private static bool IsPointBlocked(Vector2 point)
        {
            _pointer = _pointer ?? new PointerEventData(EventSystem.current);
            _pointer.position = point;

            Hits.Clear();
            EventSystem.current.RaycastAll(_pointer, Hits);

            return Hits.Count > 0;
        }

        private void UntouchSelectAtomInPlayback(VrPlaybackRenderer playback)
        {
            if (atomSelectedInfo != null) selectionManager.ActiveSelection.OnUntouchAtom(atomSelectedInfo);
        }

        private void TouchSelectAtomInPlayback(VrPlaybackRenderer playback, int id)
        {
            atomSelectedInfo = new AtomColliderEventInfo
            {
                AtomId = id,
                ColliderId = 0
            };
            //Highlight atoms to be selected.
            selectionManager.ActiveSelection.OnTouchAtom(atomSelectedInfo, VrAtomSelectionManager.GroupSelection);
            //Immediately add them to the selection.
            selectionManager.ActiveSelection.ToggleHighlightedIntoSelection(atomSelectedInfo.ColliderId);
        }

    }
}