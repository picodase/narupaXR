﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Transport.Variables.Interaction;
using Narupa.VR.Player.Control;
using NSB.MMD.RenderingTypes;
using NSB.Utility;
using UnityEngine;

namespace Narupa.VR.Renderers.Interaction
{
    /// <summary>
    /// Controls the instantiation of <see cref="InteractionRenderer"/>.
    /// </summary>
    public class InteractionRendererController : MonoBehaviour
    {
        /// <summary>
        /// Source of frames.
        /// </summary>
        public FrameSource FrameSource;
        
        private InstancePool<InteractionForceInfo> interactions;

        [SerializeField] private InstancePoolSetup interactionSetup;

        // Use this for initialization
        private void Start()
        {
            interactions = interactionSetup.Finalise<InteractionForceInfo>(false);
        }

        // Update is called once per frame
        private void Update()
        {
            var frame = FrameSource?.Component.Frame;
            if (frame == null)
                return;

            var info = frame.InteractionForceInfo;
            if (info == null)
                interactions.Clear();
            else
                interactions.SetActive(info);
        }
    }
}