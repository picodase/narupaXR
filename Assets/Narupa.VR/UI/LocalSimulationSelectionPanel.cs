﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Narupa.Client.Network;
using Narupa.VR.Player.Control;
using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI
{
	/// <summary>
	///     Class for selection from a list of preset simulations on the server.
	/// </summary>
	/// <remarks>
	///     The paths are relative to the binary location of the server.
	/// </remarks>
	public class LocalSimulationSelectionPanel : MonoBehaviour
    {
        [SerializeField] private NetworkManager networkManager;

        private bool isLoadingSimulation;
        private bool loadedSimulation;

        private HashSet<string> simulationPaths = new HashSet<string>();

        private InstancePool<string> simulations;

        [SerializeField] private InstancePoolSetup simulationsSetup;

        // Update is called once per frame
        private void Update()
        {
            if (isLoadingSimulation)
            {
                lock (simulationPaths)
                {
                    foreach (var path in simulationPaths)
                    {
                        var button = simulations.Get(path).GetComponent<Button>();
                        if (button != null)
                            button.interactable = false;
                    }
                }

                isLoadingSimulation = false;
            }
            else if (loadedSimulation)
            {
                lock (simulationPaths)
                {
                    foreach (var path in simulationPaths)
                    {
                        var button = simulations.Get(path).GetComponent<Button>();
                        if (button != null)
                            button.interactable = true;
                    }
                }

                loadedSimulation = false;
            }
        }


        private void Awake()
        {
            simulations = simulationsSetup.Finalise<string>(true);
            networkManager = FindObjectOfType<NetworkManager>();
        }

        private void Start()
        {
            var playback = FindObjectOfType<VrPlaybackRenderer>();
            playback.OnReady += OnReady;
        }

        private void OnReady()
        {
            loadedSimulation = true;
        }

        private void OnEnable()
        {
            if (!networkManager.Connected)
                return;
            lock (simulationPaths)
            {
                simulationPaths.Clear();
                foreach (var simulation in networkManager.CurrentConnection.AvailableSimulations)
                {
                    AddSimulation(simulation);
                }
                simulations.SetActive(simulationPaths);
            }
            isLoadingSimulation = false;
            loadedSimulation = true;
        }

	    /// <summary>
	    ///     Transmits a given filepath to a simulation to the server.
	    /// </summary>
	    /// <param name="config"> The simulation path.</param>
	    public void SelectSimulation(string config)
	    {
	        networkManager.CurrentConnection?.OpenSimulation(config);
            isLoadingSimulation = true;
        }

        public void AddSimulation(string simulationName)
        {
            lock (simulationPaths)
            {
                string fullSimulationPath = $"^/Assets/Simulations/{simulationName}.xml";
                simulationPaths.Add(fullSimulationPath);
            }
        }
    }
}