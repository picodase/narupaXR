﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.Primitive
{
	/// <summary>
	/// 	Draws a circle in the UI
	/// </summary>
	public class UiCircle : Graphic
	{
		/// <summary>
		/// 	From where should the fill be drawn from
		/// </summary>
		public enum FillAnchor
		{
			Center,
			Left,
			Right
		}

	    [Range(0, 1)] public float FillAmount;
	    
        public bool Fill = true;
	    
        public float Thickness = 5;
	    
        [Range(0, 360)]
        public int Segments = 360;

		public FillAnchor Anchor = FillAnchor.Center;

		protected override void OnPopulateMesh(VertexHelper vbo)
		{
			vbo.Clear();
			float diameter = (rectTransform.rect.width < rectTransform.rect.height ? rectTransform.rect.width : rectTransform.rect.height); //correct for padding and always fit RectTransform

			float outer = rectTransform.pivot.x * diameter;
			float inner = rectTransform.pivot.x * diameter - Thickness;

			float a0 = 0f; // initial angle
			float spannedAngle = FillAmount * Mathf.PI * 2f;
			
			// Adjust depending on anchor
			switch (this.Anchor)
			{
					case FillAnchor.Center:
						a0 -= spannedAngle / 2f;
						break;
					case FillAnchor.Right:
						a0 -= spannedAngle;
						break;
			}

			int totalSegments = (int) (this.Segments * FillAmount + 2);

			float da = spannedAngle / totalSegments;
			Vector2 pos0;
			Vector2 pos1;
			Vector2 pos2;
			Vector2 pos3;
			Vector2 uv0 = new Vector2(0, 0);
			Vector2 uv1 = new Vector2(0, 1);
			Vector2 uv2 = new Vector2(1, 1);
			Vector2 uv3 = new Vector2(1, 0);
			for (int i = 0; i < totalSegments; i++)
			{
				float a1 = a0 + da * i;
				float a2 = a0 + da * (i + 1);
				
				var r1 = new Vector2(Mathf.Sin(a1), Mathf.Cos(a1));
				var r2 = new Vector2(Mathf.Sin(a2), Mathf.Cos(a2));

				pos0 = r1 * outer;
				pos1 = r2 * outer;
				if (Fill)
				{
					pos2 = Vector2.zero;
					pos3 = Vector2.zero;
				}
				else
				{
					pos2 = r2 * inner;
					pos3 = r1 * inner;
				}
				uv0 = new Vector2(0, 1);
				uv1 = new Vector2(1, 1);
				uv2 = new Vector2(1, 0);
				uv3 = new Vector2(0, 0);
				vbo.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }));
			}
			

		}

        protected virtual UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs)
        {
            UIVertex[] vbo = new UIVertex[4];
            for (int i = 0; i < vertices.Length; i++)
            {
                var vert = UIVertex.simpleVert;
                vert.color = color;
                vert.position = vertices[i];
                vert.uv0 = uvs[i];
                vbo[i] = vert;
            }
            return vbo;
        }
	}
}
