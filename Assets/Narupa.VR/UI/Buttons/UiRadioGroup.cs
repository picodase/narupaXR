// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Narupa.VR.UI.Buttons
{
    /// <summary>
    ///     Wrapper around a unity ToggleGroup. Exposes a unityevent for changing value
    /// </summary>
    [RequireComponent(typeof(ToggleGroup))]
    public class UiRadioGroup : MonoBehaviour
    {
        private ToggleGroup toggleGroup;

        [System.Serializable]
        public class RadioGroupEventArgs : EventArgs
        {
            public Toggle OldToggle;
            public Toggle NewToggle;

            public RadioGroupEventArgs(Toggle old, Toggle nw)
            {
                this.OldToggle = old;
                this.NewToggle = nw;
            }
        }

        [System.Serializable]
        public class ValueChangedEvent : UnityEvent<object, RadioGroupEventArgs>
        {
            
        }

        private Toggle activeToggle = null;

        public ValueChangedEvent OnValueChanged;

        private void Awake()
        {
            toggleGroup = GetComponent<ToggleGroup>();
            foreach (var toggle in this.GetComponentsInChildren<UiToggleButton>())
                toggle.OnValueChanged.AddListener(OnToggleChange);
        }

        void OnToggleChange(object sender, UiToggleButton.ToggleButtonEventArgs args)
        {
            var newToggle = toggleGroup.ActiveToggles().FirstOrDefault();
            var oldToggle = activeToggle;
            if (newToggle != oldToggle)
            {
                activeToggle = newToggle;
                //OnValueChanged?.Invoke(this, new RadioGroupEventArgs(oldToggle, newToggle));
            }
        }
    }
}