﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Narupa.VR.UI.Selection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI
{
    public class ResidueButtonManager : MonoBehaviour
    {
        private List<GameObject> additionalSelectButtons;
        private Transform contentWindow;
        private List<ResidueSelectionController.ResidueSelectData> residuesData;
        [SerializeField] private ResidueSelectionController residueSelectionController;
        private GameObject selectButtonDefault;

        // Use this for initialization
        private void Awake()
        {
            residuesData = new List<ResidueSelectionController.ResidueSelectData>();
            additionalSelectButtons = new List<GameObject>();
            contentWindow = transform.Find("Viewport").Find("Content");
            selectButtonDefault = contentWindow.Find("SelectButton").gameObject;
            selectButtonDefault.GetComponent<ResidueSelectionButton>().ButtonId = 0;
        }

        public void UpdateResidueData(string search)
        {
            residuesData = residueSelectionController.FindResidues(search);
            GenerateButtons();
        }

        public void ToggleResidue(int buttonId, bool toggle)
        {
            //Don't try and toggle an empty list
            if (residuesData != null)
            {
                var residueSelectData = residuesData[buttonId];
                residueSelectionController.ToggleResidue(residueSelectData.AtomIndexes, toggle);
            }
        }

        //Code for reading input from number buttons
        private void GenerateButtons()
        {
            //If resiude data is null, then tell user no residues could be found
            if (residuesData == null)
            {
                //Remove all instances of SelectButton clones
                foreach (var selectButton in additionalSelectButtons) Destroy(selectButton);
                additionalSelectButtons.Clear();

                //Disable toggle and update text 
                var text = selectButtonDefault.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                text.text = "No residue found for '" + NumInputManager.InputValue + "'";
                text.color = Color.black;

                var toggle = selectButtonDefault.GetComponent<Toggle>();
                toggle.isOn = false;
                toggle.enabled = false;
                var cont = contentWindow.GetComponent<RectTransform>();
                cont.sizeDelta = new Vector2(cont.sizeDelta.x, 120);
            }
            else
            {
                //Edit the default button 
                if (!selectButtonDefault.GetComponent<Toggle>().enabled)
                    selectButtonDefault.GetComponent<Toggle>().enabled = true;
                UpdateButton(selectButtonDefault, residuesData[0]);

                //Reset additional selectButtons
                foreach (var selectButton in additionalSelectButtons) Destroy(selectButton);
                additionalSelectButtons.Clear();

                //Create additional selectButtons, if needed
                for (var i = 1; i < residuesData.Count; i++)
                {
                    var obj = Instantiate(selectButtonDefault, contentWindow);
                    obj.GetComponent<ResidueSelectionButton>().ButtonId = i;
                    UpdateButton(obj, residuesData[i]);

                    additionalSelectButtons.Add(obj);
                }

                //Resize results box
                var cont = contentWindow.GetComponent<RectTransform>();
                var requiredSize = selectButtonDefault.GetComponent<RectTransform>().sizeDelta.y * residuesData.Count;
                if (requiredSize * residuesData.Count > 120)
                    cont.sizeDelta = new Vector2(cont.sizeDelta.x, requiredSize + 1);
                else cont.sizeDelta = new Vector2(cont.sizeDelta.x, 120);
            }
        }

        private void UpdateButton(GameObject obj, ResidueSelectionController.ResidueSelectData resSelectData)
        {
            //Set text of button
            var text = obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            text.text = resSelectData.IdString.Replace(" ", "\n");
            text.color = Color.white;
            //Set whether or not button should is on/off according to whether it is already selected
            obj.GetComponent<Toggle>().isOn = resSelectData.AllSelected;
        }

        //Need to reset if disabled
        private void OnDisable()
        {
            //Destroy all created buttons
            foreach (var selectButton in additionalSelectButtons) Destroy(selectButton);
            additionalSelectButtons.Clear();
            //Reset residuesData
            residuesData = null;
        }
    }
}