﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using UnityEngine;

namespace Narupa.VR.UI.Tooltips
{
    public class MessageTooltipManager : MonoBehaviour
    {
        private uint currentMessage;
        private bool initialised;

        private readonly float lerpTime = 0.1f;

        [SerializeField] private MessageTooltip messageTooltip;
        // Use this for initialization

        private Vector3 normalScale;

        private void Awake()
        {
            initialised = true;
            normalScale = messageTooltip.transform.localScale;
            messageTooltip.gameObject.SetActive(false);
        }

        private IEnumerator DisplayMessageRoutine(float time = 1f)
        {
            messageTooltip.transform.localScale = normalScale;
            if (messageTooltip.gameObject.activeInHierarchy == false)
            {
                messageTooltip.gameObject.SetActive(true);
                iTween.ScaleFrom(messageTooltip.gameObject, Vector3.zero, lerpTime);
            }

            yield return new WaitForSeconds(time);
            iTween.ScaleTo(messageTooltip.gameObject, Vector3.zero, lerpTime);
            messageTooltip.gameObject.SetActive(false);
        }

        public void HideTooltip(uint messageId)
        {
            if (initialised == false)
                return;
            if (messageId == currentMessage)
            {
                StopAllCoroutines();
                messageTooltip.transform.localScale = normalScale;
                messageTooltip.gameObject.SetActive(false);
            }
        }

        public uint DisplayTooltip(Transform target, string title, string message, float time = 3f,
            bool drawLine = false)
        {
            StopAllCoroutines();
            messageTooltip.SetMessage(target, title, message, drawLine);
            StartCoroutine(DisplayMessageRoutine(time));
            return currentMessage++;
        }

        public void HideAllTooltips()
        {
            StopAllCoroutines();
            messageTooltip.transform.localScale = normalScale;
            messageTooltip.gameObject.SetActive(false);
        }
    }
}