﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.MaterialPalette;
using UnityEngine;

namespace Narupa.VR.UI.Tooltips
{
    /// <summary>
    ///     Draws a line between specified transforms and can be set to always face the player.
    /// </summary>
    /// <remarks>
    ///     Based on the <see cref="VRTK_ObjectTooltip" />, but only handles the line drawing and facing.
    /// </remarks>
    public class ObjectTooltip : MonoBehaviour
    {
        /// <summary>
        /// Whether to always face headset.
        /// </summary>
        [Tooltip("If this is checked then the tooltip will be rotated so it always face the headset.")]
        public bool AlwaysFaceHeadset;

        /// <summary>
        /// An optional transform of where to start drawing the line from. 
        /// </summary>
        [Tooltip(
            "An optional transform of where to start drawing the line from. If one is not provided the centre of the tooltip is used for the initial line position.")]
        public Transform DrawLineFrom;

        /// <summary>
        /// A transform of the object that a line will be drawn to.
        /// </summary>
        [Tooltip(
            "A transform of another object in the scene that a line will be drawn from the tooltip to, this helps denote what the tooltip is in relation to. If no transform is provided and the tooltip is a child of another object, then the parent object's transform will be used as this destination position.")]
        public Transform DrawLineTo;
        
        [SerializeField]
        private bool drawLine;
        /// <summary>
        /// Whether the tooltip will follow target.
        /// </summary>
        [Tooltip("If this is set to true then the tooltip will follow the target object")]
        public bool FollowTarget;

        [SerializeField] private float followTime = 1f;

        public Transform Headset;

        private Vector3 lerpVelocity;

        protected LineRenderer Line;

        public class ObjectTooltipEventArgs : EventArgs
        {
            public string newText;
        }

        /// <summary>
        /// The line colour.
        /// </summary>
        [Tooltip("The colour to use for the line drawn between the tooltip and the destination transform.")]
        public Color LineColor = Color.black;

        /// <summary>
        /// The line width.
        /// </summary>
        [Tooltip("The width of the line drawn between the tooltip and the destination transform.")]
        public float LineWidth = 0.001f;

        /// <summary>
        ///     Vector from the target to the tooltip to be maintained.
        /// </summary>
        private Vector3 tooltipDelta;


        /// <summary>
        ///     Emitted when the object tooltip is reset.
        /// </summary>
        public event EventHandler<ObjectTooltipEventArgs> ObjectTooltipReset;

        /// <summary>
        ///     Emitted when the object tooltip text is updated.
        /// </summary>
        public event EventHandler<ObjectTooltipEventArgs> ObjectTooltipTextUpdated;

        /// <summary>
        /// Fires object tooltip reset event.
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnObjectTooltipReset(ObjectTooltipEventArgs e)
        {
            if (ObjectTooltipReset != null) ObjectTooltipReset(this, e);
        }

        /// <summary>
        ///     The ResetTooltip method resets the tooltip back to its initial state.
        /// </summary>
        public virtual void ResetTooltip()
        {
            tooltipDelta = DrawLineTo.position - DrawLineFrom.position;
            SetLine();
            OnObjectTooltipReset(SetEventPayload());
        }

        /// <summary>
        /// MonoBehaviour Awake.
        /// </summary>
        protected virtual void Awake()
        {
            var palette = FindObjectOfType<GlobalMaterialPalette>();
            LineColor = new Color(palette.Palette.PrimaryColor.r, palette.Palette.PrimaryColor.g, palette.Palette.PrimaryColor.b, 0.5f);
        }

        /// <summary>
        /// MonoBehaviour OnEnable.
        /// </summary>
        protected virtual void OnEnable()
        {
            ResetTooltip();
        }

        /// <summary>
        /// MonoBehaviour OnDestroy.
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (Line != null) Line.enabled = false;
        }

        /// <summary>
        /// MonoBehaviour Update.
        /// </summary>
        protected virtual void Update()
        {
            if (FollowTarget)
                transform.position = Vector3.SmoothDamp(transform.position,
                    DrawLineTo.transform.position - tooltipDelta, ref lerpVelocity, followTime);
            DrawLine();
            if (AlwaysFaceHeadset)
            {
                //Rotate the object to look at the player.
                var relativePos = transform.position - Headset.position;
                //relativePos.y = 0f;
                var rotation = Quaternion.LookRotation(relativePos);
                transform.rotation = rotation;
                //transform.LookAt(headset);
            }
        }

        /// <summary>
        /// Sets the object tooltip payload.
        /// </summary>
        /// <param name="newText">New text to display.</param>
        /// <returns></returns>
        protected virtual ObjectTooltipEventArgs SetEventPayload(string newText = "")
        {
            return new ObjectTooltipEventArgs() { newText = newText };
        }

        /// <summary>
        /// Sets the line between the tooltip and the target transform.
        /// </summary>
        protected virtual void SetLine()
        {
            if (drawLine == false)
                return;
            if (Line == null)
            {
                Line = transform.GetComponent<LineRenderer>();

                if (Line == null)
                {
                    drawLine = false;
                    return;              
                }


            }
            Line.enabled = true;
            Line.material = Resources.Load("TooltipLine") as Material;
            if (Line.material != null) Line.material.color = LineColor;
#if UNITY_5_5_OR_NEWER
            Line.startColor = LineColor;
            Line.endColor = LineColor;
            Line.startWidth = LineWidth;
            Line.endWidth = LineWidth;
#else
            line.SetColors(lineColor, lineColor);
            line.SetWidth(lineWidth, lineWidth);
#endif
            if (DrawLineFrom == null) DrawLineFrom = transform;
        }

        /// <summary>
        /// Draws the line between the tooltip and the target transform.
        /// </summary>
        protected virtual void DrawLine()
        {
            if (DrawLineTo != null)
            {
                var fromPosition = DrawLineFrom.position;
                var toPosition = DrawLineTo.position;
                var rect = DrawLineFrom.GetComponent<RectTransform>();
                if (rect != null)
                {
                    var corners = new Vector3[4];
                    rect.GetWorldCorners(corners);
                    if (fromPosition.x < toPosition.x)
                    {
                        if (fromPosition.y > toPosition.y)
                            fromPosition = corners[0];
                        else
                            fromPosition = corners[1];
                    }
                    else
                    {
                        if (fromPosition.y < toPosition.y)
                            fromPosition = corners[2];
                        else
                            fromPosition = corners[3];
                    }
                }

                if (Line)
                {
                    Line.SetPosition(0, fromPosition);
                    Line.SetPosition(1, toPosition);
                }
            }
        }
    }
}