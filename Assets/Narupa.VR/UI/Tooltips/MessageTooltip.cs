﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using TMPro;
using UnityEngine;

namespace Narupa.VR.UI.Tooltips
{
    /// <summary>
    ///     Generic tooltip class, for displaying a simple message such as a button in VR.
    /// </summary>
    public class MessageTooltip : MonoBehaviour
    {
        private bool initialised;

        private ObjectTooltip lineDrawer;
        [SerializeField] private TextMeshProUGUI messageText;
        [SerializeField] private TextMeshProUGUI titleText;

        public void SetMessage(Transform target, string title, string message, bool drawLine)
        {
            if (!initialised) Awake();
            titleText.text = title;
            messageText.text = message;
            if (drawLine && lineDrawer != null)
            {
                lineDrawer.enabled = true;
                lineDrawer.DrawLineTo = target;
            }
            else if (lineDrawer != null)
            {
                lineDrawer.enabled = false;
            }
        }

        // Use this for initialization
        private void Awake()
        {
            lineDrawer = GetComponent<ObjectTooltip>();
            initialised = true;
        }
    }
}