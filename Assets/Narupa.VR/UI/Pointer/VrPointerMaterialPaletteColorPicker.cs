﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Client.UI.Pointer;
using Narupa.VR.MaterialPalette;
using UnityEngine;

namespace Narupa.VR.UI.Pointer
{
    /// <summary>
    ///     Assigns the correct color from the material palette for a VrPointerRay, to the ray and the ray's target
    /// </summary>
    public class VrPointerMaterialPaletteColorPicker : MaterialPaletteUser
    {
        private UiPointerRay pointerRay;

        // Use this for initialization
        private void Start()
        {
            var materialPalette = FindObjectOfType<GlobalMaterialPalette>();
            materialPalette.PaletteChanged += ((e, args) => UpdateColors());
            UpdateColors();
        }

        private void RefreshReferences()
        {
            if(pointerRay == null)
                pointerRay = GetComponentInChildren<UiPointerRay>();
        }

        public override void UpdateColors()
        {
            RefreshReferences();
            var palette = GetPalette();
            
            if (palette != null && Application.isPlaying) // Only alter materials during runtime
            {
                pointerRay.Ray.material.color = palette.SecondaryColor;
                pointerRay.RayTarget.material.color = palette.SecondaryColor;
            }
        }
    }
}