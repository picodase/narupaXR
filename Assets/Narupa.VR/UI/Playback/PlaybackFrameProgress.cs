﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Player.Control;
using Narupa.VR.UI.Radial;
using UnityEngine;

namespace Narupa.VR.UI.Playback
{
	public class PlaybackFrameProgress : MonoBehaviour
	{
	
		private VrPlaybackRenderer playbackRenderer;
		private UiRadialProgressBar progressBar;

		// Use this for initialization
		void Awake ()
		{
			playbackRenderer = FindObjectOfType<VrPlaybackRenderer>();
			this.progressBar = GetComponent<UiRadialProgressBar>();
		}
	
		// Update is called once per frame
		void Update ()
		{
			this.progressBar.value = playbackRenderer.CurrentFrame / playbackRenderer.History.Count;
		}
	}
}
