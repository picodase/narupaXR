﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.VR.Controller;
using Narupa.VR.Player.Control;
using UnityEngine;

namespace Narupa.VR.UI.Playback
{
	
	/// <summary>
	/// 	Switches between two possible UIs depending on the current playback mode
	/// </summary>
	public class VrPlaybackModeController : MonoBehaviour
	{

		[SerializeField] private GameObject playingUI;
		
		[SerializeField] private GameObject pausedUI;

		private VrPlaybackRenderer playbackRenderer;

		private void Awake()
		{
			playbackRenderer = FindObjectOfType<VrPlaybackRenderer>();
			playbackRenderer.PlaybackPlayModeChanged += UpdateMenus;
			FindObjectOfType<VrControllerStateManager>().ModeStart += OnModeStart;
		}

		private void Start()
		{
			UpdateMenus(playbackRenderer.PlaybackPlayMode);
		}

		private void OnModeStart(object sender, VrControllerStateManager.ControllerModeChangedEventArgs e)
		{
			UpdateMenus(playbackRenderer.PlaybackPlayMode);
		}

		private void UpdateMenus(bool isPlaying)
		{
			var conditionals = playingUI.GetComponentsInParent<VrControllerModeConditional>();
			foreach(var cond in conditionals)
				if (!cond.GetValue())
					return;
			playingUI.gameObject.SetActive(isPlaying);
			pausedUI.gameObject.SetActive(!isPlaying);
		}
		
	}
}
