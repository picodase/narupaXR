﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.IO;
using NSB.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI
{
    public class LocalSimulationElement : InstanceView<string>
    {
        [SerializeField] private TextMeshProUGUI descriptionText;

        [SerializeField] private LocalSimulationSelectionPanel panel;

        [SerializeField] private Button selectButton;

        private void Awake()
        {
            selectButton.onClick.AddListener(() => panel.SelectSimulation(config));
        }

        protected override void Configure()
        {
            var text = Path.GetFileNameWithoutExtension(config);
            if (text == null) return;
            text = text.ToLower();
            descriptionText.text = text;
        }
    }
}