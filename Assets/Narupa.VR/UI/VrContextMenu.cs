﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for switching between canvases in VR.
    /// </summary>
    public class VrContextMenu : MonoBehaviour
    {
        private readonly HashSet<VrCanvas> canvases = new HashSet<VrCanvas>();
        private bool initialised;

        //Stores history of previous canvases so we can work our way back out of menus.
        private readonly List<VrCanvas> previousCanvases = new List<VrCanvas>();
        
        /// <summary>
        /// Time to delay before switching.
        /// </summary>
        public float SwitchTimeDelay = 0.1f;

        /// <summary>
        ///     The previous canvas, if one exists.
        /// </summary>
        public VrCanvas PreviousCanvas { get; private set; }

        public event EventHandler SwitchedToCanvas;

        [SerializeField] [Tooltip("Canvases to ignore.")]
        private VrCanvas[] canvasToIgnore;
        
        [SerializeField]
        private bool hideCanvasesOnStart = true;
        private void Start()
        {
            if (initialised == false)
                Initialise();
        }

        private void Initialise()
        {
            var allChildrenCanvases = GetComponentsInChildren<VrCanvas>();
            //Only get the canvases that are a direct child of the context menu.
            foreach (var canvas in allChildrenCanvases)
                if (canvas.transform.parent == transform)
                {
                    if(canvasToIgnore.Contains(canvas))
                        continue;
                    
                    canvases.Add(canvas);
                    canvas.gameObject.SetActive(!hideCanvasesOnStart);
                    
                }
            initialised = true;
        }

        /// <summary>
        ///     Switch to the specified canvas.
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="overrideReturn">
        ///     Instead of setting the current canvas to be the back target, override it with a specific
        ///     canvas.
        /// </param>
        /// <returns></returns>
        public bool SwitchToCanvas(VrCanvas canvas, VrCanvas overrideReturn = null)
        {
            if (!initialised) Initialise();
            if (canvases.Contains(canvas) == false) return false;
            if (overrideReturn != null) PreviousCanvas = overrideReturn;

            canvas.StopAllCoroutines();
            StartCoroutine(SwitchToCanvasRoutine(canvas, overrideReturn));
            if (SwitchedToCanvas != null) SwitchedToCanvas(canvas, null);
            return true;
        }

        public void SwitchToCanvasEditor(VrCanvas canvas)
        {
            SwitchToCanvas(canvas);
        }

        /// <summary>
        /// Register a canvas to be controlled by this instance.
        /// </summary>
        /// <param name="canvas"></param>
        public void RegisterCanvas(VrCanvas canvas)
        {
            canvases.Add(canvas);
        }

        /// <summary>
        /// Deregister a canvas.
        /// </summary>
        /// <param name="canvas"></param>
        public void UnregisterCanvas(VrCanvas canvas)
        {
            canvases.Remove(canvas);
        }

        /// <summary>
        ///     Animation routine for switching to a canvas.
        /// </summary>
        /// <param name="canvas">Canvas to switch to.</param>
        /// <param name="canvasToReturnTo">Canvas to return to.</param>
        /// <returns></returns>
        /// <remarks>
        ///     Activates the disable routine for any active child canvases, then switches to the target canvas.
        /// </remarks>
        private IEnumerator SwitchToCanvasRoutine(VrCanvas canvas, VrCanvas canvasToReturnTo = null,
            bool storePreviousCanvas = true)
        {
            var switchingCanvas = false;

            switchingCanvas = HideAll(canvasToReturnTo == null && storePreviousCanvas ? true : false);
            if (switchingCanvas)
                yield return new WaitForSeconds(SwitchTimeDelay);
            canvas.gameObject.SetActive(true);
        }

        /// <summary>
        ///     Hides all active canvases attached to this menu.
        /// </summary>
        /// <returns>Returns <c>true</c> if a canvas has started the processing of hiding.</returns>
        internal bool HideAll(bool storePreviousCanvas = true)
        {
            var hidingCanvas = false;
            foreach (var childrenCanvas in canvases)
                if (childrenCanvas.gameObject.activeInHierarchy)
                {
                    StartCoroutine(childrenCanvas.DisableRoutine(SwitchTimeDelay));
                    hidingCanvas = true;
                    if (storePreviousCanvas) PreviousCanvas = childrenCanvas;
                }

            if (storePreviousCanvas && hidingCanvas)
                previousCanvases.Add(PreviousCanvas);

            return hidingCanvas;
        }

        /// <summary>
        /// Switches to the previous canvas.
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void SwitchToPreviousCanvas()
        {
            if (!initialised) Initialise();
            if (PreviousCanvas == null) throw new Exception("There is no previous canvas to return to!");
            StartCoroutine(SwitchToCanvasRoutine(PreviousCanvas, null, false));
            previousCanvases.Remove(PreviousCanvas);
            if (previousCanvases.Count > 0)
                PreviousCanvas = previousCanvases[previousCanvases.Count - 1];
            else
                PreviousCanvas = null;
        }
    }
}