﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.VR.UI.Selection
{
    /// <summary>
    ///     Class for handling the button for creating a new selection.
    /// </summary>
    public class CreateSelectionButton : VrButtonBase
    {
        /// <summary>
        ///     Event for new selection button click.
        /// </summary>
        public event EventHandler CreateSelectionClicked;

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
        }

        protected override void Awake()
        {
            base.Awake();
            ButtonClicked += OnButtonClicked;
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            if (CreateSelectionClicked != null) CreateSelectionClicked(this, null);
        }
    }
}