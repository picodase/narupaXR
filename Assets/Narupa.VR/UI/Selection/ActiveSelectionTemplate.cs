// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Selection;
using NSB.MMD.Base;
using NSB.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.Selection
{
    public class ActiveSelectionTemplate : InstanceView<InteractiveSelection>
    {
        /// <summary>
        ///     Whether we interact with the base layer
        /// </summary>
        /// <remarks>
        ///     TODO Get rid of this and make base layer a proper selection.
        /// </remarks>
        public static bool InteractWithBaseLayer = true;

        public static bool BaseLayerVisible = true;

        /// <summary>
        ///     Root for default rendering.
        /// </summary>
        private RendererManager baseLayerRenderer;

        private DeleteSelectionButton deleteButton;

        /// <summary>
        ///     The selection associated with this item.
        /// </summary>
        public InteractiveSelection Selection;

        private SelectionManager selectionManager;

        [SerializeField] private SelectionSwitchManager selectionSwitchManager;

        private ActiveSelectionToggle toggle;

        [SerializeField] private Toggle visualiseSelection;

        private void Awake()
        {
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
            if (selectionSwitchManager == null) selectionSwitchManager = FindObjectOfType<SelectionSwitchManager>();
            if (selectionSwitchManager == null)
                throw new NullReferenceException("Missing SelectionSwitchManager! Set in inspector.");
            toggle = GetComponentInChildren<ActiveSelectionToggle>();
            toggle.SelectionHighlighted += selectionSwitchManager.OnSelectionHighlighted;
            toggle.SelectionUnhighlighted += selectionSwitchManager.OnSelectionUnhighlighted;
            toggle.SelectionChosen += selectionSwitchManager.OnSelectionChosen;
            deleteButton = GetComponentInChildren<DeleteSelectionButton>();
            if (deleteButton != null) deleteButton.DeleteSelectionClicked += selectionSwitchManager.OnSelectionDeleted;
            visualiseSelection.onValueChanged.AddListener(OnVisualiseSelectionChange);
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
        }

        private void Start()
        {
            baseLayerRenderer = selectionManager.BaseLayerRendererManager;
        }

        private void OnVisualiseSelectionChange(bool arg0)
        {
            //If toggle is on, hide visualisation.
            if (Selection != null)
            {
                Selection.RendererRoot.gameObject.SetActive(!arg0);
            }
            //If selection is null, then this button is for the default renderer, so hide/show it as appropriate.
            else
            {
                baseLayerRenderer.gameObject.SetActive(!arg0);
                BaseLayerVisible = !arg0;
            }
        }

        protected override void Configure()
        {
            if (toggle == null) Awake();
            if (selectionSwitchManager == null) selectionSwitchManager = GetComponentInParent<SelectionSwitchManager>();
            if (selectionManager == null) selectionManager = FindObjectOfType<SelectionManager>();
            var activeSelection = false;
            if (selectionManager != null)
                if (selectionManager.ActiveSelection != null)
                    activeSelection = selectionManager.ActiveSelection == config;
            toggle.SetSelection(config, activeSelection);
            deleteButton.Selection = config;
            Selection = config;
            //Add toggle to the toggle group.
            //toggle.GetComponent<Toggle>().group = selectionSwitchManager.GetComponent<ToggleGroup>();
        }
    }
}