﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using NSB.MMD.Base;
using UnityEngine;

namespace NSB.MMD.Renders
{
    public class BoundingBoxRenderer : MMDRenderer
    {
        [System.Serializable]
        public class BoundsConfig : RendererConfig
        {
            public FloatParameter Thickness;

            protected override void Setup()
            {
                Thickness = AddFloat("Thickness", 1, dirtying: false);
            }
        }

        public BoundsConfig Settings = new BoundsConfig();
        public override RendererConfig Config => Settings;

        private List<Transform> edges;
        private SlimMath.BoundingBox bounds;

        private void Awake()
        {
            var renderers = GetComponentsInChildren<MeshRenderer>(true);

            if (renderers.Length != 12)
            {
                Debug.LogError("Bounding box has invalid edges.", this);
                return;
            }

            edges = renderers.Select(edge => edge.transform).ToList();
        }

        public override void Refresh()
        {
            if (Frame == null) return;

            bounds = Frame.GetVariable<SlimMath.BoundingBox>(Nano.Transport.Variables.VariableName.SimBoundingBox);

            Vector3 min = new Vector3(bounds.Minimum.X, bounds.Minimum.Y, bounds.Minimum.Z);
            Vector3 max = new Vector3(bounds.Maximum.X, bounds.Maximum.Y, bounds.Maximum.Z);

            Vector3 size = max - min;

            Vector3 center = (size * 0.5f) + min;

            float thickness = Settings.Thickness.Value * size.magnitude;
            size += new Vector3(thickness, thickness, thickness);

            SetEdge(0, new Vector3(min.x, max.y, center.z), new Vector3(thickness, thickness, size.z));
            SetEdge(1, new Vector3(min.x, center.y, max.z), new Vector3(thickness, size.y, thickness));
            SetEdge(2, new Vector3(min.x, min.y, center.z), new Vector3(thickness, thickness, size.z));
            SetEdge(3, new Vector3(min.x, center.y, min.z), new Vector3(thickness, size.y, thickness));

            SetEdge(4, new Vector3(max.x, max.y, center.z), new Vector3(thickness, thickness, size.z));
            SetEdge(5, new Vector3(max.x, center.y, max.z), new Vector3(thickness, size.y, thickness));
            SetEdge(6, new Vector3(max.x, min.y, center.z), new Vector3(thickness, thickness, size.z));
            SetEdge(7, new Vector3(max.x, center.y, min.z), new Vector3(thickness, size.y, thickness));

            SetEdge(8, new Vector3(center.x, min.y, min.z), new Vector3(size.x, thickness, thickness));
            SetEdge(9, new Vector3(center.x, min.y, max.z), new Vector3(size.x, thickness, thickness));
            SetEdge(10, new Vector3(center.x, max.y, min.z), new Vector3(size.x, thickness, thickness));
            SetEdge(11, new Vector3(center.x, max.y, max.z), new Vector3(size.x, thickness, thickness));
        }

        private void SetEdge(int index, Vector3 center, Vector3 size)
        {
            edges[index].localPosition = center;
            edges[index].localScale = size;
        }

        public void OnDrawGizmos()
        {
            var center = (bounds.Minimum + bounds.Maximum) * 0.5f;
            var size = bounds.Maximum - bounds.Minimum;

            Gizmos.DrawWireCube(new Vector3(center.X, center.Y, center.Z), 
                new Vector3(size.X, size.Y, size.Z));
        }
    }
}
