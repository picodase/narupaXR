﻿#ifndef DRAWING_PALETTE_INCLUDED
#define DRAWING_PALETTE_INCLUDED

inline fixed4 pal2Dindex(sampler2D palette, float index)
{
    // sample color from palette texture (with corrections)
    fixed4 col = tex2D(palette, float2(index + 1 / 512., .5));

	return col;
}

inline fixed4 pal2Dtex(sampler2D palette, sampler2D tex, float2 uv)
{
    // sample texture
    fixed4 col = tex2D(tex, uv);
    // return corresponding colour from the palette
    return pal2Dindex(palette, col.a);
}

#endif