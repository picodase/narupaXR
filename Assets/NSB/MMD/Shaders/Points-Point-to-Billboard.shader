﻿Shader "NSB/MMD/Points-Point-Billboard"
{
    Properties
    {
		_MainTex ("Texture", 2D) = "pink" {}
		_Scaling ("Scaling", Range(0, 1)) = 0.333
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry"
            "IgnoreProjector" = "True"
            "RenderType" = "Opaque"
            "PreviewType" = "Cube"
        }

        Cull Off
        Lighting Off
        ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom
			#pragma multi_compile_fog
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION; 
				float4 texcoord : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 color : COLOR;
				float4 uv : TEXCOORD0;

				UNITY_FOG_COORDS(1)
            };

			sampler2D _MainTex;
			float _Scaling;

            v2f vert(appdata v)
            {
                v2f o;

                o.pos =  mul(unity_ObjectToWorld, v.vertex);
                o.color = v.color;
				o.uv = v.texcoord;

				UNITY_TRANSFER_FOG(o, UnityObjectToClipPos(v.vertex));

                return o;
            }

            [maxvertexcount(4)]
            void geom(point v2f p[1], inout TriangleStream<v2f> triStream)
            {
                float3 look = _WorldSpaceCameraPos - p[0].pos;
				float3 up = normalize(cross(look, float3(0, 1, 0)));
                float3 right = normalize(cross(up, look));
 
                float halfS = p[0].uv.x * _Scaling;
                           
                float4 v0 = float4(p[0].pos + halfS * right - halfS * up, 1.0);
                float4 v1 = float4(p[0].pos + halfS * right + halfS * up, 1.0);
                float4 v2 = float4(p[0].pos - halfS * right - halfS * up, 1.0);
                float4 v3 = float4(p[0].pos - halfS * right + halfS * up, 1.0);
 
				// TODO: *infomercial voice* there has to be a better way!
				v0 = mul(unity_WorldToObject, v0); 
				v1 = mul(unity_WorldToObject, v1); 
				v2 = mul(unity_WorldToObject, v2); 
				v3 = mul(unity_WorldToObject, v3); 

				v2f pIn = p[0];
                pIn.pos = UnityObjectToClipPos(v0);
                pIn.uv = float4(1.0, 0.0, 0.0, 0.0);
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v1);
                pIn.uv = float4(1.0, 1.0, 0.0, 0.0);
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v2);
                pIn.uv = float4(0.0, 0.0, 0.0, 0.0);
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v3);
                pIn.uv = float4(0.0, 1.0, 0.0, 0.0);
                triStream.Append(pIn);
            }

            half4 frag(v2f i) : COLOR
            {
				half4 tex = tex2D(_MainTex, i.uv.xy);

				clip(tex.a - .9);

				half4 c = tex * i.color;

				UNITY_APPLY_FOG(i.fogCoord, c);
				UNITY_OPAQUE_ALPHA(c.a);

				return c;
            }
            ENDCG
        }
    }
}
