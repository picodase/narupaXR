﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Renders;
using NSB.MMD.Styles;
using NSB.Processing;
using UnityEngine;
using UnityEngine.Assertions;

namespace NSB.MMD.Base
{
    /// <summary>
    /// Manages renderers.
    /// </summary>
    public class RendererManager : MonoBehaviour
    {
        /// <summary>
        /// MMDRenderers that are the direct children of this script.
        /// </summary>
        public List<MMDRenderer> Renderers = new List<MMDRenderer>();

        /// <summary>
        /// The default renderer for this object.
        /// </summary>
        [Tooltip("Default renderer for this rendering node.")]
        [SerializeField]
        public MMDRenderer DefaultRenderer;

        private int previousRendererIndex = -1;
        private int currentRendererIndex;

        public MMDRenderer ActiveRenderer;

        public FrameSource FrameSource;

        public SelectionSource SelectionSource;
        public StyleSource DefaultStyleSource;

        /// <summary>
        /// Source for max radius style.
        /// </summary>
        /// <remarks>
        /// Used to update the given max radius style based on this renderer manager's current renderer. 
        /// </remarks>
        [Tooltip("Max Radius style this renderer manager should update.")]
        public StyleSource MaxRadiusStyleSource;
        /// <summary>
        /// Whether this instance is the base layer.
        /// </summary>
        public bool BaseLayer;

        /// <summary>
        /// The default style applied to all renderers.
        /// </summary>
        [Tooltip("Default style applied to all renderers.")]
        public NSBStyle Style;

        private MaxRadiusStyle maxRadiusStyle;

        private BoundingBoxRenderer boundingBoxRenderer;
        private bool initialised;

        // Use this for initialization
        private void Start()
        {
            if(!initialised)
                Initialise();
        }

        public void Initialise()
        {
            int i = 0;
            foreach (Transform obj in transform)
            {
                var childRenderer = obj.gameObject.GetComponent<MMDRenderer>();
                if (childRenderer == null) continue;
                if (childRenderer is BoundingBoxRenderer)
                {
                    boundingBoxRenderer = childRenderer as BoundingBoxRenderer;
                }
                else
                    Renderers.Add(childRenderer);
                if (childRenderer != DefaultRenderer)
                {
                    childRenderer.gameObject.SetActive(false);
                }
                else
                {
                    currentRendererIndex = i;
                    ActiveRenderer = childRenderer;
                    ActiveRenderer.AutoRefresh = false;
                }
                i++;
            }

            if (MaxRadiusStyleSource == null) maxRadiusStyle = FindObjectOfType<MaxRadiusStyle>();
            else maxRadiusStyle = MaxRadiusStyleSource.Component as MaxRadiusStyle;
            Style = DefaultStyleSource.Component.Style;
            SetRenderer(DefaultRenderer);
            initialised = true;
        }
    
        private void Update()
        {
            ActiveRenderer.Frame = FrameSource.Component.Frame;
            ActiveRenderer.Selection = SelectionSource.Component.Selection;
            ActiveRenderer.Style = Style;
            ActiveRenderer.Refresh();
            if(boundingBoxRenderer != null) boundingBoxRenderer.Frame = FrameSource.Component.Frame;
        }

        private void DisableAll()
        {
            foreach (var r in Renderers)
            {
                r.gameObject.SetActive(false);
                if (maxRadiusStyle != null)
                    maxRadiusStyle.Renderers.Remove(r);
            }
        }

        internal void SetRenderStyle(NSBStyle style)
        {
            Style = style;
        }

        /// <summary>
        /// Set the renderer to the index specified.
        /// </summary>
        /// <param name="i"></param>
        public void SetRenderer(int i)
        {
            DisableAll();
            Assert.IsTrue(i < Renderers.Count && i >= 0);
            Renderers[i].gameObject.SetActive(true);
            previousRendererIndex = currentRendererIndex;
            currentRendererIndex = i;
            ActiveRenderer = Renderers[i];
            maxRadiusStyle?.Renderers.Add(ActiveRenderer);
            //Switch to the target renderers style?
            /*
        if (ActiveRenderer.UseSources)
        {
            Style = ActiveRenderer.StyleSource.Component.Style;
        }
        else
        {
            Style = ActiveRenderer.Style;
        }
        */
        }

        /// <summary>
        /// Set the renderer to the specified GameObject.
        /// </summary>
        /// <param name="config"></param>
        internal void SetRenderer(MMDRenderer config)
        {
            int index = Renderers.IndexOf(config);
            Assert.IsTrue(index >= 0);
            SetRenderer(index);
        }

        /// <summary>
        /// Switch to the previous renderer.
        /// </summary>
        internal void SwitchToPreviousRenderer()
        {
            if (previousRendererIndex != -1)
                SetRenderer(previousRendererIndex);
            else
                SetRenderer(DefaultRenderer);
        }

        /// <summary>
        /// Sets the renderer by renderer name. 
        /// </summary>
        /// <param name="selectionInfoRenderer">Name of the renderer to switch to.</param>
        /// <remarks>If there is no renderer with the specified name, the rednerer will not change.</remarks>
        public void SetRenderer(string selectionInfoRenderer)
        {
            foreach (var render in Renderers)
            {
                if (!String.Equals(render.name, selectionInfoRenderer, StringComparison.CurrentCultureIgnoreCase)) continue;
                SetRenderer(render);
                return;
            }
            Debug.LogWarning($"Unknown renderer, have you initialised the RendererManager? {selectionInfoRenderer}");
        }
    }
}