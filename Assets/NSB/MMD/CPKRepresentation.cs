﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using UnityEngine;

namespace NSB.MMD
{
    public class CPKRepresentation : MMDRenderer
    {
        [System.Serializable]
        public class CPKConfig : RendererConfig
        {
            public IntParameter AtomMeshComplexity;
            public BoolParameter AtomSpin;
            public BoolParameter AtomHemispheres;
            public FloatParameter AtomHemisphereCull;
            public FloatParameter AtomScaling;

            public IntParameter BondPrismSides;
            public FloatParameter BondRadius;
            public FloatParameter BondSharpness;

            protected override void Setup()
            {
                AtomMeshComplexity = AddInt(nameof(AtomMeshComplexity), 6, 0, 12);
                AtomSpin = AddBool(nameof(AtomSpin), false);
                AtomHemispheres = AddBool(nameof(AtomHemispheres), false);
                AtomHemisphereCull = AddFloat(nameof(AtomHemisphereCull), 0, 0, 1);
                AtomScaling = AddFloat(nameof(AtomScaling), .333f, 0, 2, dirtying: false);

                BondPrismSides = AddInt(nameof(BondPrismSides), 3, 3, 16);
                BondRadius = AddFloat(nameof(BondRadius), 0.04f, 0, 0.1f, dirtying: false);
                BondSharpness = AddFloat("Bond Blending", 0.5f, 0, 1, dirtying: false);
            }
        }

        [Header("Setup")]
        [SerializeField]
        private VDWRepresentation atoms;

        [SerializeField]
        private BondsRepresentation bonds;
    
        public CPKConfig Settings = new CPKConfig();
        public override RendererConfig Config => Settings;

        public override void Refresh()
        {
            atoms.Frame = Frame;
            atoms.Selection = Selection;
            atoms.Style = Style;
        
            atoms.Settings.MeshComplexity.Value = Settings.AtomMeshComplexity.Value;
            atoms.Settings.Hemispheres.Value = Settings.AtomHemispheres.Value;
            atoms.Settings.HemisphereCull.Value = Settings.AtomHemisphereCull.Value;
            atoms.Settings.Spin.Value = Settings.AtomSpin.Value;
            atoms.Settings.Scaling.Value = Settings.AtomScaling.Value;

            bonds.Frame = Frame;
            bonds.Selection = Selection;
            bonds.Style = Style;

            bonds.Settings.BlendSharpness.Value = Settings.BondSharpness.Value;
            bonds.Settings.EndCaps.Value = false;
            bonds.Settings.PrismSides.Value = Settings.BondPrismSides.Value;
            bonds.Settings.Radius.Value = Settings.BondRadius.Value;

            atoms.Refresh();
            bonds.Refresh();
        }

        public override float GetAtomBoundingRadius(int id)
        {
            return atoms.GetAtomBoundingRadius(id);
        }
    }
}