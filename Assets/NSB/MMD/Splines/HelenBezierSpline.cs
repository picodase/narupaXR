﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD.Splines
{
    public static class HelenBezierSpline 
    {
        public static int GetPointCount(int pathCount, int intermediates)
        {
            int posts = ((pathCount - 1) / 3) + 1;

            return posts + (posts - 1) * intermediates;
        }

        public static void ComputeLine(List<Vector3> controls,
            List<Vector3> vertices,
            List<Vector3> normals,
            int intermediates)
        {
            Profiler.BeginSample("HelenBezierSpline.ComputeLine");

            vertices.Resize(GetPointCount(controls.Count, intermediates));
            normals.Resize(GetPointCount(controls.Count, intermediates));

            int posts = ((controls.Count - 1) / 3) + 1;
            int stride = intermediates + 1;
            float inv = 1f / stride;
            Vector3 up = Vector3.right;

            //for (int i = 0; i < posts - 1; ++i)
            Parallel.For(0, posts - 1, i =>
            {
                Vector3 p0 = controls[i * 3 + 0];
                Vector3 p1 = controls[i * 3 + 1];
                Vector3 p2 = controls[i * 3 + 2];
                Vector3 p3 = controls[i * 3 + 3];

                vertices[i * stride] = p0;

                for (int j = 1; j < stride; ++j)
                {
                    float t = j * inv;
                    float v = 1 - t;

                    float v2 = v * v;
                    float v3 = v2 * v;

                    float t2 = t * t;
                    float t3 = t2 * t;

                    Vector3 S = FasterMath.Add(FasterMath.Mul(p0, v3),
                        FasterMath.Mul(p1, v2 * t * 3),
                        FasterMath.Mul(p2, t2 * v * 3),
                        FasterMath.Mul(p3, t3));

                    vertices[i * stride + j] = S;
                    normals[i * stride + j] = getNormal(p0, p1, p2, p3, t, up);
                }
            });

            Profiler.EndSample();
        }

        public static void Smooth(List<Vector3> points, 
            List<Vector3> normals,
            int smoothInterations, 
            int smoothFactor)
        {
            int startValue = 0;
            if (smoothFactor % 2 == 0) smoothFactor++;
            int offset = (smoothFactor - 1) / 2;

            float inv = 1f / smoothFactor;

            for (int i = 0; i < smoothInterations; i++)
            {
                for (int j = 0; j < points.Count - offset; j++)
                {
                    if (j <= offset) startValue = -j;
                    else startValue = -offset;

                    for (int k = startValue; k < (smoothFactor + startValue); k++)
                    {
                        if (k != 0)
                        {
                            points[j] += points[j + k];
                            normals[j] += normals[j + k];
                        }
                    }

                    points[j] *= inv;
                    normals[j] = (normals[j] * inv).normalized;
                }
            }
        }

        //Code for getTangent and getNormal adapted from: https://www.youtube.com/watch?v=o9RK6O2kOKo
        private static Vector3 getTangent(Vector3 P0, Vector3 P1, Vector3 P2, Vector3 P3, float t)
        {
            float v = 1f - t;
            float v2 = v * v;
            float t2 = t * t;

            Vector3 tangent = FasterMath.Add(FasterMath.Mul(P0,     -v2),
                FasterMath.Mul(P1,  3 * v2 - 2 * v),
                FasterMath.Mul(P2, -3 * t2 + 2 * t),
                FasterMath.Mul(P3,      t2));

            return tangent.normalized;
        }

        private static Vector3 getNormal(Vector3 P0, Vector3 P1, Vector3 P2, Vector3 P3, float t, Vector3 up)
        {
            Vector3 tng = getTangent(P0, P1, P2, P3, t);
            Vector3 binormal = Vector3.Cross(up, tng).normalized;
            return Vector3.Cross(tng, binormal);
        }
    }
}
