﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Client;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD
{
    public class BondsRepresentation : MMDRenderer
    {
        [System.Serializable]
        public class BondsConfig : RendererConfig
        {
            public IntParameter Darken;
            public IntParameter PrismSides;
            public BoolParameter EndCaps;
            public FloatParameter Radius;
            public FloatParameter BlendSharpness;

            protected override void Setup()
            {
                Darken = AddInt("Darken", 64, 0, 255);
                PrismSides = AddInt(nameof(PrismSides), 3, 3, 16);
                EndCaps = AddBool(nameof(EndCaps), false);
                Radius = AddFloat(nameof(Radius), 0.04f, 0, 0.1f, dirtying: false);
                BlendSharpness = AddFloat("Blend Sharpness", 0.5f, 0, 1, false);
            }
        }

        [Header("Shaders")]
        [SerializeField]
        private Shader bondsShader;
        private Material bondsMaterial;

        [Header("Setup")]
        public BondsConfig Settings = new BondsConfig();
        public override RendererConfig Config => Settings;
    
        private ParticleBatchSet particleBatches = new ParticleBatchSet();
        private ParticleBatchRenderer particleRenderer = new ParticleBatchRenderer();

        private void Awake()
        {
            bondsMaterial = new Material(bondsShader);
        }

        private List<BondPair> filteredBonds = new List<BondPair>();

        public override void Refresh()
        {
            if (!CheckValidity()) return;

            Profiler.BeginSample("Refresh BondsRepresentation");

            if (!Selection.FilterBonds(Frame, filteredBonds))
            {
                particleRenderer.Clear();
                return;
            }

            if (Settings.ResetDirty()) RegenerateMesh();

            if (Style.PaletteIsValid)
            {
                bondsMaterial.EnableKeyword("GRADIENT");
                Style.UsePalette(bondsMaterial);
            }
            else
            {
                bondsMaterial.DisableKeyword("GRADIENT");
            }

            bondsMaterial.SetFloat("_Sharpness", Settings.BlendSharpness.Value);

            particleBatches.GenerateParticles(this, filteredBonds, UpdateParticle);
            particleRenderer.RenderBatchSet(particleBatches);
            Profiler.EndSample();
        }

        public static void UpdateParticle(BondsRepresentation data,
            BondPair bond,
            ref ParticleSystem.Particle particle)
        {
            Vector3 posA = data.Frame.AtomPositions[bond.A];
            Vector3 posB = data.Frame.AtomPositions[bond.B];

            Color32 color = default(Color32);

            if (data.Style.PaletteIsValid)
            {
                UpdateParticleData(data.Style, bond, (byte) data.Settings.Darken.Value, ref color);
                particle.startColor = color;
            }
            else
            {
                var highlighted = data.Style.HighlightedAtoms;

                bool highlight = highlighted != null
                                 && highlighted.Count > 0
                                 && (!highlighted.Contains(bond.A) || !highlighted.Contains(bond.B));

                Color blend = Color32.LerpUnclamped(data.Style.AtomColors[bond.A],
                    data.Style.AtomColors[bond.B],
                    0.5f);

                float mult = highlight ? data.Settings.Darken.Value / 255f : 1;

                particle.startColor = FasterMath.Mul(blend, mult);
            }

            Vector3 dir = FasterMath.Sub(posB, posA);
            Vector3 rot = Quaternion.LookRotation(dir).eulerAngles;

            particle.position = FasterMath.Lerp(posA, posB, 0.5f);
            particle.rotation3D = rot;

            Vector3 scale;
            scale.y = data.Settings.Radius.Value;
            scale.z = dir.magnitude;
            scale.x = data.Settings.Radius.Value;

            particle.startSize3D = scale;
        }

        public static void UpdateParticleData(NSBStyle style,
            BondPair bond,
            byte darken,
            ref Color32 data)
        {
            var highlighted = style.HighlightedAtoms;

            bool highlightA = highlighted != null
                              && highlighted.Count > 0
                              && !highlighted.Contains(bond.A);

            bool highlightB = highlighted != null
                              && highlighted.Count > 0
                              && !highlighted.Contains(bond.B);

            ushort elementA = style.AtomPaletteIndices[bond.A];
            ushort elementB = style.AtomPaletteIndices[bond.B];

            data.r = (byte)(elementA);
            data.g = (byte)(elementB);
            data.b = (byte)(highlightA ? darken : 255);
            data.a = (byte)(highlightB ? darken : 255);
        }

        private void RegenerateMesh()
        {
            var mesh = new Mesh();
            MeshGeneration.MeshGeneration.GenerateBondPrism(mesh, Settings.PrismSides.Value, capped: Settings.EndCaps.Value);
            particleRenderer.Initialise(transform);
            particleRenderer.SetMesh(mesh);
            particleRenderer.SetMaterial(bondsMaterial);
            particleBatches.SetBatchSize(ParticleBatchRenderer.VertexLimit / mesh.vertexCount);
        }

        public override float GetAtomBoundingRadius(int id)
        {
            return Settings.Radius.Value * 0.5f;
        }
    }
}