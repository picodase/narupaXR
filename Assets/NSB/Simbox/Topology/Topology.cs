﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Rug.Osc;

namespace NSB.Simbox.Topology
{
    /// <summary>
    ///     Represents an atomic topology for use in client side visualisation and interaction.
    /// </summary>
    public class Topology
    {
        /// <summary>
        ///     The set of atom groups.
        /// </summary>
        public Dictionary<string, AtomGroup> AtomGroups = new Dictionary<string, AtomGroup>();

        /// <summary>
        ///     The atoms in the topology, accessed by index.
        /// </summary>
        public List<TopologyAtom> AtomsByAbsoluteIndex = new List<TopologyAtom>();

        /// <summary>
        ///     The atoms in the topology, accessed by address
        /// </summary>
        public Dictionary<string, TopologyAtom> AtomsByAddress = new Dictionary<string, TopologyAtom>();

        private readonly Dictionary<TopologyAtom, int> atomToVisibleIndexMap = new Dictionary<TopologyAtom, int>();

        public Action OnReady;


        private readonly object topologyLock = new object();

        /// <summary>
        ///     A map from the visible atoms indices to the underlying topology.
        /// </summary>
        private readonly List<TopologyAtom> visibleAtomMap2 = new List<TopologyAtom>();

        /// <summary>
        /// Constructor for a topology.
        /// </summary>
        public Topology()
        {
        }

        /// <summary>
        /// Construct topology from set of addresses.
        /// </summary>
        /// <param name="atomicAddresses"></param>
        public Topology(IEnumerable<string> atomicAddresses)
        {
            var i = 0;
            foreach (var address in atomicAddresses) AddGroupsFromAddress(address, i++);
        }

        
        /// <summary>
        ///     The visible atoms in the topology.
        /// </summary>
        public List<TopologyAtom> VisibleAtomMap
        {
            get
            {
                if (Ready) return visibleAtomMap2;
                return null;
            }
        }

        /// <summary>
        ///     A map from a <see cref="TopologyAtom" /> to its index visible atoms list, if it is visible.
        /// </summary>
        public Dictionary<TopologyAtom, int> AtomToVisibleIndexMap
        {
            get
            {
                if (Ready) return atomToVisibleIndexMap;
                return null;
            }
        }

        /// <summary>
        ///     Indicates whether the topology is ready to be used.
        /// </summary>
        public bool Ready { get; private set; }

        private void AddGroupsFromAddress(string address, int absoluteIndex)
        {
            lock (topologyLock)
            {
                var addressOsc = new OscAddress(address);

                AddGroupsFromAddress(addressOsc, absoluteIndex);
            }
        }

        private void AddGroupsFromAddress(OscAddress addressOsc, int absoluteIndex)
        {
            //Start after the /top bit.
            var addressParts = addressOsc[0].Value + addressOsc[1].Value;
            var groupsInAddress = new List<AtomGroup>();
            var isProtein = IsAddressPartOfProtein(addressOsc);
            // We know that the protein strings have to end in the same style, so can count when they start.
            var proteinPartStart = addressOsc.Count - 5;
            //Bits to collect if the thing is a protein.
            AtomGroup chain = null;
            AtomGroup residue = null;
            int resId;

            AtomGroup prevAtomGroup = null;

            for (var i = 3; i < addressOsc.Count; i += 2)
            {
                var skipNextPart = false;

                var groupName = addressOsc[i].Value;
                addressParts += "/" + addressOsc[i].Value;
                var groupAddress = addressParts;

                //If this is the last bit of the address, then it's the atom.
                if (i == addressOsc.Count - 1)
                {
                    TopologyAtom atom;
                    //If it's a protein, spawn our special protein atom which has more information.
                    if (isProtein)
                    {
                        //If it's a protein, then the atom part of address might have be of form CA-99-1000, 
                        //where CA is the name, 99 is the sequence number, and 1000 is the actual index (incase it is greater
                        //than 100000
                        //Take the first field, the name, as the groupName.
                        groupName = groupName.Split('-')[0];

                        var proteinAtom = new ProteinAtom(prevAtomGroup, addressParts, absoluteIndex, groupName, chain,
                            residue);
                        atom = proteinAtom;
                    }
                    //Otherwise just spawn a boring old atom.
                    else
                    {
                        atom = new TopologyAtom(prevAtomGroup, addressParts, absoluteIndex, groupName);
                    }

                    AddAtomToTopology(atom, groupsInAddress);
                    return;
                }

                //Check whether the next part after this is an integer, if is then process both parts. Example //Water/0/**
                var nextAddressPart = addressOsc[i + 2].Value;

                int intParse;

                if (int.TryParse(nextAddressPart, out intParse))

                {
                    groupAddress = groupAddress + "/" + nextAddressPart;
                    //Skip our address part index forward so we don't create a group from the numeric part
                    addressParts = groupAddress;
                    skipNextPart = true;
                }

                AtomGroup group;
                //Next thing to do is work out if we've already seen this group, and create it if not.
                if (AtomGroups.ContainsKey(groupAddress) == false)
                {
                    group = new AtomGroup(groupAddress, groupName, intParse, prevAtomGroup);
                    AtomGroups.Add(groupAddress, group);
                }

                group = AtomGroups[groupAddress];
                groupsInAddress.Add(group);

                //Handle collection of protein specific bits from the address.
                if (isProtein && i >= proteinPartStart)
                {
                    if (i == proteinPartStart)
                    {
                        chain = group;
                    }

                    //If this bit is the residue part (which looks like "ALA-3-3") then extract the index.
                    if (i == proteinPartStart + 2)
                    {
                        residue = group;
                        var resNameAndId = groupName.Split('-');
                        resId = int.Parse(resNameAndId[1]);
                        group.Id = resId;
                    }
                }

                //Assign the group to be the previous one, so we can set the parent correctly for the next part.
                prevAtomGroup = group;

                if (skipNextPart) i += 2;
            }
        }

        /// <summary>
        ///     Adds the atom to the various maps and lists, and adds it to all the atom groups it belongs to.
        /// </summary>
        /// <param name="atom"> The atom to add to the topology. </param>
        /// <param name="groupsInAddress"> The groups this atom belongs to. </param>
        private void AddAtomToTopology(TopologyAtom atom, List<AtomGroup> groupsInAddress)
        {
            for (var i = 0; i < groupsInAddress.Count; ++i)
            {
                var group = groupsInAddress[i];

                group.Atoms.Add(atom);
                // Add children to the groups
                if (i < groupsInAddress.Count - 1)
                    group.Children.Add(groupsInAddress[i + 1]);
            }

            for (var i = AtomsByAbsoluteIndex.Count; i < atom.Index + 1; i++)
                AtomsByAbsoluteIndex.Add(new TopologyAtom());

            AtomsByAbsoluteIndex[atom.Index] = atom;
            AtomsByAddress[atom.Address] = atom;
        }

        /// <summary>
        ///     Determines whether the address is a protein address.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        /// <remarks>
        ///     Proteins have a specific address layout, for example:
        ///     /sim/2-ala/0/A/ALA-1-1/C-1-1
        ///     We can detect a protein by checking that the second to last address part matches the style residueName-resID.
        /// </remarks>
        private bool IsAddressPartOfProtein(OscAddress address)
        {
            var proteinPattern = "[a-zA-Z][a-zA-Z][a-zA-Z]-[0-9]*-[0-9]*";

            if (Regex.IsMatch(address[address.Count - 3].Value, proteinPattern)) return true;
            return false;
        }

        internal void AddAddress(string address, params object[] parameters)
        {
            AddGroupsFromAddress(address, (int) parameters[0]);
        }

        /// <summary>
        ///     Returns the atoms
        /// </summary>
        /// <param name="visibleIndex"></param>
        /// <returns></returns>
        public List<int> GetAtomsInGroup(int visibleIndex)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Returns the set of atoms that are within parentCount steps from the atom in the tree.
        /// </summary>
        /// <param name="visibleIndex">Index of the atom in the visible topology</param>
        /// <param name="parentCount"></param>
        /// <param name="atoms"></param>
        /// <returns></returns>
        internal HashSet<int> GetAtoms(int visibleIndex, int parentCount, HashSet<int> atoms = null)
        {
            if (atoms == null) atoms = new HashSet<int>();

            if (Ready == false) return atoms;

            var atom = visibleAtomMap2[visibleIndex];
            atoms.Add(visibleIndex);
            if (parentCount == 0) return atoms;

            //Loop up to the relevant parent, or at least as far as we can.
            var parent = atom.Parent;

            for (var i = 1; i < parentCount; i++)
            {
                if (parent.Parent == null) break;
                parent = parent.Parent;
            }

            return GetVisibleAtomsInGroup(parent, atoms);
        }

        internal HashSet<int> GetVisibleAtomsInGroup(AtomGroup group, HashSet<int> atoms = null)
        {
            if (atoms == null) atoms = new HashSet<int>();
            if (Ready == false) return atoms;
            foreach (var tAtom in group.Atoms)
                //If no visible atom map has been set up, then all atoms are visible by their absolute index.
                if (atomToVisibleIndexMap.Count == 0)
                    atoms.Add(tAtom.Index);
                else if (atomToVisibleIndexMap.ContainsKey(tAtom)) atoms.Add(atomToVisibleIndexMap[tAtom]);
            return atoms;
        }

        public HashSet<int> GetAtomsInParent(int visibleIndex)
        {
            throw new NotImplementedException();
        }

        public void UpdateVisibleAtomMap(int[] visibleAtomMap)
        {
            if (Ready == false)
                throw new Exception(
                    "Topology is not ready yet, we haven't received all the data necessary to form the visible atom map!");

            lock (topologyLock)
            {
                //If visible atom map is empty and our local version doesn't contain all atoms, re-evaluate.
                if (visibleAtomMap.Length == 0)
                {
                    if (visibleAtomMap2.Count != AtomsByAbsoluteIndex.Count)
                    {
                        visibleAtomMap2.Clear();
                        for (var i = 0; i < AtomsByAbsoluteIndex.Count; i++)
                        {
                            var atom = AtomsByAbsoluteIndex[i];

                            visibleAtomMap2.Add(atom);
                            atomToVisibleIndexMap[atom] = i;
                        }
                    }
                }
                else if (visibleAtomMap.Length != visibleAtomMap2.Count)
                {
                    visibleAtomMap2.Clear();
                    for (var i = 0; i < visibleAtomMap.Length; i++)
                    {
                        var abs = visibleAtomMap[i];
                        var atom = AtomsByAbsoluteIndex[abs];

                        visibleAtomMap2.Add(atom);
                        atomToVisibleIndexMap[atom] = i;
                    }
                }
            }
        }

        public void FinalizeTopology()
        {
            lock (topologyLock)
            {
                Ready = true;
                OnReady?.Invoke();
            }
        }
    }
}