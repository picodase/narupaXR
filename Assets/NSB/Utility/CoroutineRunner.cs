﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections;
using UnityEngine;

namespace NSB.Utility
{
    public class CoroutineRunner : MonoBehaviour 
    {
        public static Coroutine Create(IEnumerator routine, string name = "Coroutine")
        {
            var obj = new GameObject(name);
            var runner = obj.AddComponent<CoroutineRunner>();

            return runner.Run(routine);
        }

        private Coroutine Run(IEnumerator routine)
        {
            return StartCoroutine(RunCO(routine));
        }

        private IEnumerator RunCO(IEnumerator routine)
        {
            yield return StartCoroutine(routine);

            Destroy(gameObject);
        }
    }
}
