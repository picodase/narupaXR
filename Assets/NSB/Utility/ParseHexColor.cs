﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Globalization;
using UnityEngine;

namespace NSB.Utility
{
    public static class ParseHexColors
    {
        public static Color ParseHexColor(this string hex)
        {
            Color32 color = Color.magenta;

            if (hex.Length == 6)
            {
                byte.TryParse(hex.Substring(0, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out color.r);
                byte.TryParse(hex.Substring(2, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out color.g);
                byte.TryParse(hex.Substring(4, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out color.b);
            }

            return color;
        }
    }
}
