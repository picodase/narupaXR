﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Utility
{
    /// <summary>
    /// Interface representing a pooled object that can be reused to display 
    /// different instances of the same kind of data.
    /// </summary>
    public interface IConfigView<TConfig>
    {
        /// <summary>
        /// The currently configuration data
        /// </summary>
        TConfig config { get; }

        /// <summary>
        /// Replace existing configuration data with a new one
        /// </summary>
        void SetConfig(TConfig config);

        /// <summary>
        /// First-time setup to run after this pooled object is created
        /// </summary>
        void Setup();
    
        /// <summary>
        /// Routine to run to reset this object so it is ready to be reused
        /// </summary>
        void Cleanup();

        /// <summary>
        /// Routine to run to update this object from the existing configuration
        /// data (which may have been modified in-place)
        /// </summary>
        void Refresh();
    }

    /// <summary>
    /// Base implementation for IConfigView types that are MonoBehaviours
    /// </summary>
    public abstract class InstanceView<TConfig> : MonoBehaviour, IConfigView<TConfig>
    {
        public TConfig config { get; private set; }

        /// <summary>
        /// Replace the existing configuration and defer displaying it to the
        /// `Configure` method.
        /// </summary>
        public void SetConfig(TConfig config)
        {
            this.config = config;

            Configure();
        }

        /// <summary>
        /// Configure this InstanceView to display the current config for the first
        /// time. In this case we defer to the `Refresh` method instead.
        /// </summary>
        protected virtual void Configure() { Refresh(); }

        public virtual void Setup() { }
        public virtual void Cleanup() { }
        public virtual void Refresh() { }
    }
}