﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using NSB.Examples.Player.Control;
using NSB.Utility;
using UnityEngine;

namespace NSB.Examples.Player.Screens.Replay_Select
{
    public class ReplayInfo
    {
        public TextAsset asset;
    }

    public class ReplaySelectionPanel : MonoBehaviour
    {
        [SerializeField]
        private ExampleAppController app;
        [SerializeField]
        private InstancePoolSetup replaysSetup;
        private InstancePool<ReplayInfo> replays;

        [SerializeField]
        private List<TextAsset> trajectoryAssets = new List<TextAsset>();

        public void SelectReplay(ReplayInfo info)
        {
            app.EnterRecording(info.asset);
        }

        private void Awake()
        {
            replays = replaysSetup.Finalise<ReplayInfo>(sort: true);
        }

        private void OnEnable()
        {
            replays.SetActive(trajectoryAssets.Select(asset => new ReplayInfo { asset = asset }).ToList());
        }
    }
}