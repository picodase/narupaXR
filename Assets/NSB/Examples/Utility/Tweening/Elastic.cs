﻿using System;

namespace NSB.Examples.Utility.Tweening
{
    class Elastic : IEaser
    {
        protected static float a = 1f;
		protected static float p = 0.3f;
		protected static float s = p / 4f;

        public float EaseIn(float ratio)
        {
            if (ratio == 0f || ratio == 1f) { return ratio; }
            return -(a * (float)Math.Pow(2f, 10f * (ratio -= 1f)) * (float)Math.Sin((ratio - s) * (2f * Math.PI) / p));
        }

        public float EaseOut(float ratio)
        {
            if (ratio == 0f || ratio == 1) { return ratio; }
            return a * (float)Math.Pow(2f, -10f * ratio) * (float)Math.Sin((ratio - s) * (2f * Math.PI) / p) + 1;
        }

        public float EaseInOut(float ratio)
        {
            if (ratio == 0 || ratio == 1) { return ratio; }
            ratio = ratio * 2f - 1f;

            if (ratio < 0)
            {
                return -0.5f * (a * (float)Math.Pow(2f, 10f * ratio) * (float)Math.Sin((ratio - s * 1.5f) * (2f * Math.PI) / (p * 1.5f)));
            }
            return 0.5f * a * (float)Math.Pow(2f, -10f * ratio) * (float)Math.Sin((ratio - s * 1.5f) * (2f * Math.PI) / (p * 1.5f)) + 1f;
        }
    }
}
