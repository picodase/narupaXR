﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Narupa.Utility.Unity;

public static class TestAsserts
{
    private static string Vector3ToStringPrecise(Vector3 vector)
    {
        return $"({vector.x}, {vector.y}, {vector.z})";
    }

    public static void AreApproximatelyEqual(Vector3 a, Vector3 b, float tolerance = 0.0001f)
    {
        var error = b - a;

        if (Mathf.Abs(error.x) > tolerance
         || Mathf.Abs(error.y) > tolerance 
         || Mathf.Abs(error.z) > tolerance)
        {
            string aPrecise = Vector3ToStringPrecise(a);
            string bPrecise = Vector3ToStringPrecise(b);
            string errorPrecise = Vector3ToStringPrecise(error);

            Assert.Fail($"Vectors not per component equal within {tolerance}:\n{aPrecise}\n{bPrecise}\nerror:\n{errorPrecise}");
        }
    }

    public static void AreApproximatelyEqual(Matrix4x4 matrix1, 
                                             Matrix4x4 matrix2,
                                             float tolerance = 0.000001f)
    {
        Matrix4x4 error = default(Matrix4x4);
        bool fail = false;

        for (int i = 0; i < 16; ++i)
        {
            error[i] = matrix2[i] - matrix1[i];

            fail |= Mathf.Abs(error[i]) > tolerance;
        }

        if (fail)
        {
            Assert.Fail("Matrices not per component equal within {tolerance}:\n{matrix1}\n{matrix2}\nerror:\n{error}");
        }
    }

    public static void IsMatrixUniformScale(Matrix4x4 matrix, float tolerance = 0.00001f)
    {
        var scale = matrix.GetScale();

        if (Mathf.Abs(scale.x - scale.y) > tolerance
         || Mathf.Abs(scale.y - scale.z) > tolerance)
        {
            Assert.Fail($"Matrix does not have uniform scale within {tolerance}:\n{matrix}\n{Vector3ToStringPrecise(scale)}");
        }
    }
}
