﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Narupa.Utility.Unity;
using System;

using Random = UnityEngine.Random;
using System.Linq;

public class TransformGesturesTests
{
    private static Matrix4x4 GetRandomTRSMatrix(Vector3? translation = null,
                                                Quaternion? rotation = null,
                                                Vector3? scale = null)
    {
        return Matrix4x4.TRS(translation ?? Random.onUnitSphere, 
                             rotation ?? Random.rotation, 
                             scale ?? Vector3.one * Random.value);
    }

    private static Matrix4x4 GetRandomControlPoint()
    {
        return GetRandomTRSMatrix(scale: Vector3.one);
    }

    [Test, Pairwise]
    public void OnePointGesture_WithRandomInput_PreservesScale(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TwoPointGestureSequences))] 
        TwoPointGestureTestSequence sequence)
    {
        var gesture = new OnePointTranslateRotateGesture();

        var controlPoint = sequence.ControlPointsSequence[0].LeftController;
        var initialObjectMatrix = sequence.InitialObjectMatrix;
        float referenceScale = initialObjectMatrix.GetScale().x;

        gesture.BeginGesture(initialObjectMatrix, controlPoint);

        foreach (var pair in sequence.ControlPointsSequence.Skip(1))
        {
            var matrix = gesture.UpdateControlPoint(pair.LeftController);

            TestAsserts.IsMatrixUniformScale(matrix);
            TestAsserts.AreApproximatelyEqual(matrix.GetScale(), Vector3.one * referenceScale);
        }
    }

    [Test, Pairwise]
    public void TwoPointGesture_WithRandomInput_ScalesOnRelativeSeparation(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TwoPointGestureSequences))] 
        TwoPointGestureTestSequence sequence)
    {
        var gesture = new TwoPointTranslateRotateScaleGesture();

        var initialControlPoint1 = sequence.ControlPointsSequence[0].LeftController;
        var initialControlPoint2 = sequence.ControlPointsSequence[0].RightController;
        var initialSeparation = Vector3.Distance(initialControlPoint1.GetTranslation(),
                                                 initialControlPoint2.GetTranslation());
        var initialObjectMatrix = sequence.InitialObjectMatrix;
        float referenceScale = initialObjectMatrix.GetScale().x;

        gesture.BeginGesture(initialObjectMatrix,
                             initialControlPoint1,
                             initialControlPoint2);

        foreach (var pair in sequence.ControlPointsSequence.Skip(1))
        {
            var updatedControlPoint1 = pair.LeftController;
            var updatedControlPoint2 = pair.RightController;
            var updatedSeparation = Vector3.Distance(updatedControlPoint1.GetTranslation(),
                                                     updatedControlPoint2.GetTranslation());
            var expectedScale = referenceScale * (updatedSeparation / initialSeparation);

            var matrix = gesture.UpdateControlPoints(updatedControlPoint1, updatedControlPoint2);

            TestAsserts.IsMatrixUniformScale(matrix);
            TestAsserts.AreApproximatelyEqual(matrix.GetScale(), Vector3.one * expectedScale);
        }
    }

    [Test]
    public void TwoPointGesture_WithRandomInput_PreservesPointsLocalPosition(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TwoPointGestureSequences))] 
        TwoPointGestureTestSequence sequence)
    {
        var gesture = new TwoPointTranslateRotateScaleGesture();

        var initialControlPoint1 = sequence.ControlPointsSequence[0].LeftController;
        var initialControlPoint2 = sequence.ControlPointsSequence[1].RightController;
        var initialObjectMatrix = sequence.InitialObjectMatrix;
        
        var objectControlPoint1 = initialObjectMatrix.inverse.MultiplyPoint3x4(initialControlPoint1.GetTranslation());
        var objectControlPoint2 = initialObjectMatrix.inverse.MultiplyPoint3x4(initialControlPoint2.GetTranslation());

        gesture.BeginGesture(initialObjectMatrix,
                             initialControlPoint1,
                             initialControlPoint2);

        foreach (var pair in sequence.ControlPointsSequence.Skip(1))
        {
            var updatedControlPoint1 = pair.LeftController;
            var updatedControlPoint2 = pair.RightController;

            var updatedObjectMatrix = gesture.UpdateControlPoints(updatedControlPoint1, updatedControlPoint2);
            
            var updatedObjectControlPoint1 = updatedObjectMatrix.inverse.MultiplyPoint3x4(updatedControlPoint1.GetTranslation());
            var updatedObjectControlPoint2 = updatedObjectMatrix.inverse.MultiplyPoint3x4(updatedControlPoint2.GetTranslation());

            TestAsserts.AreApproximatelyEqual(objectControlPoint1, updatedObjectControlPoint1);
            TestAsserts.AreApproximatelyEqual(objectControlPoint2, updatedObjectControlPoint2);
        }
    }
}
