# README #
This repository contains the Narupa XR build. It supports multiplayer interactive sessions with a Narupa server. 

We currently support the HTC Vive. 

## Prerequisites 

* [Git LFS](https://git-lfs.github.com/), please ensure you have it installed. 
* The version of Unity3D specified in [ProjectVersion.txt](https://gitlab.com/glowackigroup/narupaXR/blob/master/ProjectSettings/ProjectVersion.txt), usually the latest.
* SteamVR and an HTC Vive headset.

## Getting started 

This page provides details on how to clone and build the repository from source. For more detailed guidance, check out the [documentation](https://intangiblerealities.gitlab.io/narupaXR/). Documentation on the engine for running simulations can be found
[here](https://intangiblerealities.gitlab.io/narupa/).

Make sure Git LFS has downloaded all the files by running the following command: 
```sh
git lfs fetch
git lfs checkout
```

Open the project in unity. The root project folder is `narupaXR`.

Open the NarupaVR scene, which is stored under `Narupa.XR/Scenes`. 

Connect your VR headset, turn on SteamVR and press play! 

## Building your own multi-person VR lab 

If you'd like to build your own multi-person VR lab, we've included [instructions outlining what kit you need and how to set it up](https://intangiblerealities.gitlab.io/narupaXR/md__user_guide__user_guide.html)

## Building & Contributing to the Documentation

The documentation is included with the repository, and are the markdown files in the folder `Documentation Source`. To build the documentation, the following is required: 

* [Doxygen](http://www.stack.nl/~dimitri/doxygen/). The program `doxygen` must be added to your `PATH`. On Windows, this is typically under `C:\Program Files\doxygen\bin`. 

Run the script `build_docs.bat` by clicking on it, or run `build_docs.sh` from a bash shell. The documentation will appear in the folder `Documentation`.

## Contributing to Development 

We welcome contributions in all forms. Helping us [find and fix bugs](https://gitlab.com/intangiblerealities/narupaXR/issues), improving the [documentation](https://intangiblerealities.gitlab.io/narupaXR) and suggesting features are all encouraged.
If you have a contribution that to add to the repository, please first sign our [community license agreement](https://www.simulitix.com/narupa-cla), and submit a 
[pull request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html). 
